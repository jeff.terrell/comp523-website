---
title: Assignment 10: Tech Talk
stem: tech-talk
---

*Tech talks will occur in class, 3 talks per class session, between Monday,
November 1<sup>st</sup> and Wednesday, December 1<sup>st</sup>, as listed on [the
calendar](/calendar/).*

### Due

One member of your team should submit your team's tech talk topic and top 3 date
preferences (see [the calendar](/calendar/)) by email to me
([terrell@cs.unc.edu](mailto:terrell@cs.unc.edu)) by Sunday, October
18<sup>th</sup>. I will assign presentation dates as soon as possible
afterwards. Note that, in the event that two teams want the same topic, the
first one to email me (both the topic and the date preferences) wins.

Then, on your assigned date in class, you will present your topic.

### Context

You've been learning about some cool technologies this semester in your
project. Share some of that knowledge with the class, and get practice
preparing and delivering a team presentation. (Remember that this course
carries a communications intensive (CI) label.)

### Requirements

Prepare a **20-minute** presentation on some topic that:

- is related to software development;
- has some currency; and
- will leave many (if not most) of us with some useful new information.

Each team member should participate in both the preparation and delivery of the
presentation.

By the time you deliver your talk, post your presentation (as a PPT or PDF file
or a link to a Google Slides online document) to your project web site.

For full credit, incorporate some element of *active learning* in your
presentation; that is, involve the audience somehow. Is there a way for people
to follow along with a tutorial or demo on their laptops? In the past, teams
have used [Code Sandbox](https://codesandbox.io/) or some other online code
editor ([Codepen](https://codepen.io/#), [JS Fiddle](https://jsfiddle.net/)) to
let people edit relevant code and see its result without installing any
software. Remember that you will need to share the URL with people somehow, so
it may be wise to add a link from your project web site. If your presentation
is missing an active learning element, your tech talk will receive at best a
grade of B.

### Hints

**Rehearse** as a team to get the timing right. As little as 15 minutes is
fine, or as much as 24. (We're trying to get through 3 teams in a 75-minute
class session, including swap time.) When I give talks, I sometimes rehearse
more than 10 times to really get it down. Not that you have to do that, but it
can make the difference between a decent presentation and a great one, if
you're feeling ambitious.

To give you an idea of what topics are possible, here are the topics from last
semester:

- CORS
- Firebase in Swift
- QT
- Vega-Lite
- Express.js
- Creating a chat bot
- Neo4j
- Security
- Mongo DB
- Swift
- Google Maps, Mapkit
- Expo
- Electron
- NodeJS
- Code Smells
- ReThink DB
- Angular
- Google AppScript
- Cytoscape
- Progressive Web Apps

