---
title: Fall '20 Lecture Notes - Mon Aug 31 - Design Principles
stem: f20.2020-08-31.design-principles
---

Most of this content, including the slides are due to Graham Langdon, who [gave
a guest lecture on this topic last year](https://youtu.be/-mOIsXMPUyY). Used
with permission. Thanks, Graham!

- slides from Graham ([PPTX](../f19/lectures/langdon.slides.intro-to-design.pptx), [PDF](../f20/lectures/2020-08-31.design-principles/design-principles.slides.langdon.pdf))
    - (today we got through the user experience section and stopped at testing & feedback)
- video [on YouTube](https://youtu.be/1NO954M7Of4)
- [transcript](../f20/lectures/2020-08-31.design-principles/transcript.vtt)

### Announcements

- Music today: Clair de Lune by Claude Debussy, because, for me, it achieves the 4th level of user experience
- Grades have been posted to Sakai (haven't double-checked the upload yet though)
- [Assignment 2: Web Site](../web-site/) is due this week
- [Assignment 3: User Stories](../user-stories/) is due next week

### Tips from mentors

Some mentors shared some good tips related to project management.

From Jacob Yackenovich, mentor to Team R:

- grant all teammates authority to make decisions on work items
- encourage your teammates to disagree with you/with each other if there isn't consensus
- you don't have to decide in a vacuum: you can ask for help
- you might benefit from shared work hours and having an "open zoom" meeting where everyone is working and no one is talking by default
- roll forward: if a decision turns out to be wrong, make forward progress as you change the decision

From Stacey Wright, mentor to Team U:

- project management work is 1/3 of any project, and testing is another 1/3
- (so don't stress out if PM takes some time&mdash;that's expected)

In _The Mythical Man-Month_, Fred Brooks said that actual coding time is 1/6th of the total time! The full ratios:

- 1/3rd time for planning
- 1/6th time for coding
- 1/4th time for component tests
- 1/4th time for system tests with all components in hand
