---
title: Assignment 9: Midterm Presentation
stem: midterm-presentation
---

### Due

Monday, October 18<sup>th</sup>.

I will randomly select half the teams to present on Monday, and the other half
will present on Wednesday the 20<sup>th</sup>, so be prepared to present on
Monday.

### Context

Show others how your project is coming along, and get practice presenting. For
better or worse, it's usually not enough to know something or to have done
something; you have to also be able to talk about it and show people.

### Requirements

Prepare a **5-minute max** presentation (shorter is fine) with the following
outline:

1. **Project overview** - What problem is your project solving? Who are the
   users?
2. **Architecture** - Show us your architecture diagram and tell us what
   language(s), tools, and frameworks you're using.
3. **Clickable prototype** - Demonstrate what the app will look like when it's
   done with your clickable prototype.

To minimize swapping time, the presentation should be a link to a Google Slides
presentation. Ensure that anybody with the link can access it.

Also make sure that the Figma clickable prototype is linked from your project
web site and viewable by anybody.

One of your team should have the slides ready to share by the time class starts
on Monday. I will randomly pick a team, and I want to keep setup time minimal
so that we don't run over our allotted class time.

All team members must present.

### Hints

**Rehearse** as a team to get the timing right. As little as 4 minutes is fine,
but try not to go over the 5-minute mark. (We're trying to get through 13 teams
in a 75-minute class session, including swap time.) When I give talks, I
sometimes rehearse more than 10 times to really get it down. Not that you have
to do that, but it can make the difference between a decent presentation and a
great one, if you're feeling ambitious.

