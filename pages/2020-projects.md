---
title: 2020 Projects
stem: 2020-projects
---

_These are the projects for fall 2020._ Some links may not work. You can also
see the projects for
[fall 2019](../2019-projects/) and
[fall 2021](../projects/).

### Contents

- [Projects](#projects)
- [Mentor assignments](#mentor-assignments)
- [Pitch recordings](#pitch-recordings)
- [Intellectual property](#intellectual-property)
- [Project details](#project-details)

### Projects

After a team finishes [Assignment 2](../web-site/), their web site will be
linked here.

| team | lead client | students                         | web site                                                              |
|------|-------------|----------------------------------|-----------------------------------------------------------------------|
| A    | R. Jones    | Bost, Evora, Rivkin-Fish         | [link](https://tarheels.live/nctrc/)                                  |
| B    | Verber      | Byrd, Goodwin, Ryan              | [link](https://tarheels.live/sweepstatf20/)                           |
| C    | Taylor      | Koester, Pasupula, Troutman      | [link](https://tarheels.live/comp523vrmoria/)                         |
| D    | Vanderburg  | Bunze, Goodman, Wu               | [link](http://connect-intro.herokuapp.com/)                           |
| F    | Blair       | J. Chen, Z. Chen, B. Li          | [link](https://tarheels.live/teamf/)                                  |
| G    | Carter      | Ehsan, Gupta, Jain, Zhong        | [link](https://tarheels.live/comp523ante/)                            |
| H    | Radsky      | Bluhm, Dershem, Williams         | [link](https://tarheels.live/unceasyaccessteam/)                      |
| I    | Villarreal  | Dong, Sun, Zhan                  | [link](https://tarheels.live/maptheimpacts/)                          |
| J    | Vernon      | Schwartz, L. Wheeler, M. Wheeler | [link](https://github.com/Deeakron/COMP-523-J/blob/gh-pages/index.md) |
| K    | Millberger  | Knight, Michaels, Pack           | [link](https://tarheels.live/ibiblioreporting/)                       |
| L    | Millberger  | Chang, Guan, Xu                  | [link](https://tarheels.live/ibibliobillingportal/)                   |
| M    | Ward        | Cahill, Errico, Humphrey         | [link](https://mtcahill57.github.io/523-fa20-m.github.io/)            |
| N    | Steponaitis | A. Chen, King, Vij               | [link](https://tarheels.live/comp523eot/)                             |
| O    | Halpin      | L. Li, X. Li, Parslow            | [link](https://tarheels.live/comp523peerlearning/)                    |
| R    | Hurlbert    | Le, Pan, Randby                  | [link](https://comp523fa2020.github.io/Overview/)                     |
| S    | Shah        | Eshet, Griffith, Lane            | [link](https://tarheels.live/comp523gracefully/)                      |
| T    | Scheier     | Gandecha, Kotha, Smith           | [link](https://jacobasmith.github.io/embit/)                          |
| U    | A. Jones    | Bacudio, Narayanan, Suresh       | [link](https://tarheels.live/teamu/)                                  |
| V    | McCormick   | Y. Chen, H. Li, Wei              | [link](https://tarheels.live/teamv/)                                  |
| W    | Fisher      | Foster, Fulmer, Poddutoori       | [link](https://tarheels.live/wellaware/)                              |
| X    | Morey       | Huang, Kim, Zhang                | [link](https://uncteamx.netlify.app/)                                 |


### Mentor assignments

Students, please let me know when a meeting time has been established and I will
post it here.

| team | mentor      | meeting time     |
|------|-------------|------------------|
| A    | Pollack     | Mondays 4pm      |
| B    | Chmielecki  | Wednesdays 8pm   |
| C    | Farmer      | Wednesdays 5pm   |
| D    | Woods       | Tuesdays 9am     |
| F    | Gramann     | Fridays 1pm      |
| G    | Ginsberg    | Fridays 1:30pm   |
| H    | Franzen     | Tuesdays 10am    |
| I    | Lake        | Thursdays 5:15pm |
| J    | Auger       | Fridays 5pm      |
| K    | Stump       | Wednesdays 12pm  |
| L    | Dinger      | Thursdays 4:45pm |
| M    | Fletcher    | Tuesdays 10am    |
| N    | Abzhanov    | Fridays 3pm      |
| O    | Blair       | Fridays 2:30pm   |
| R    | Yackenovich | Wednesdays 4pm   |
| S    | Brown       | Fridays 3pm      |
| T    | Vega        | Tuesdays 12pm    |
| U    | Wright      | Fridays 3pm      |
| V    | Byzek       | Thursdays 9:30am |
| W    | Mowery      | Fridays 3pm      |
| X    | Nguyen      | Fridays 4:15pm   |


### Pitch recordings

- [Fri Aug 14](https://youtu.be/26vVWlWA2rs)
- [Mon Aug 17](https://youtu.be/sFAXxV00fDU)


### Intellectual property

Clients marked with <sup>**IP**</sup> in the list below have indicated that they
are sensitive to issues of intellectual property and/or confidentiality. You can
read their response about particular concerns in their application. Such
projects will require students to review and sign a legal agreement to
participate in the project. Students are not required to participate in
IP-sensitive projects, and their grade is in no way affected by their choice of
whether to engage in such a project. The agreement has been written by the UNC
Office of Technology Commercialization (OTC) with this particular situation (and
students' needs) in mind. For more information, see the [participation
guidelines](../f20/clients/ip-guidelines.docx). You can also view the
[participation agreement](../f20/clients/ip-agreement.docx) itself. (It's
short.) For questions, please contact [Peter
Liao](https://otc.unc.edu/people/peter-liao/) &lt;liaopb@unc.edu&gt;,
Commercialization Manager at UNC OTC, who will also attend class on Wed Aug 12
to discuss these issues.


### Project details

Project titles (provided by the client) and lead client names are given in
pitch order below, with links to the client-submitted application and any
additional materials provided by the client. Project summaries (provided by
clients) are further below.

The applications are responses to [this form](https://www.tfaforms.com/4726239),
which you can see for more details on what questions are being asked, etc. You
can page through the application, but please don't submit an application.

Projects marked <sup>**iOS**</sup> are iOS-only development projects and will
require access to MacOS for development work.

|   | client                  | my title                                                                                                                                                            | details              | pitch                                             | application                           | additional materials                                                                                |
|---|-------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------|---------------------------------------------------|---------------------------------------|-----------------------------------------------------------------------------------------------------|
| A | R. Jones, Katz, Cole    | <sup>**IP**</sup> NC Therapeutic Riding Center Check-In App                                                                                                         | [link](#r-jones)     | [PPTX](../f20/clients/pitches/a.r-jones.pptx)     | [PDF](../f20/clients/r-jones.pdf)     |                                                                                                     |
| B | Verber, Dick            | <sup>**iOS**</sup>Sweepstat App                                                                                                                                     | [link](#verber)      | [PPTX](../f20/clients/pitches/b.verber.pptx)      | [PDF](../f20/clients/verber.pdf)      |                                                                                                     |
| C | Taylor                  | VR-Moria                                                                                                                                                            | [link](#taylor)      | [YouTube](https://youtu.be/qAM4vTQyDyk)           | [PDF](../f20/clients/taylor.pdf)      |                                                                                                     |
| D | Vanderburg              | <sup>**IP**</sup> Connect                                                                                                                                           | [link](#vanderburg)  | [PPTX](../f20/clients/pitches/d.vanderburg.pptx)  | [PDF](../f20/clients/vanderburg.pdf)  |                                                                                                     |
| F | Blair                   | <sup>**iOS**</sup> Software Project Analytics iOS App                                                                                                               | [link](#blair)       | -                                                 | [PDF](../f20/clients/blair.pdf)       | [PDF](../f20/clients/blair.additional.pdf)                                                          |
| G | Carter                  | ANTE                                                                                                                                                                | [link](#carter)      | [PPTX](../f20/clients/pitches/g.carter.pptx)      | [PDF](../f20/clients/carter.pdf)      | [PPTX](../f20/clients/carter.ante-presentation.pptx), [XLSX](../f20/clients/carter.additional.xlsx) |
| H | Radsky, Moon            | Easy Access                                                                                                                                                         | [link](#radsky)      | [PPTX](../f20/clients/pitches/h.radsky.pptx)      | [PDF](../f20/clients/radsky.pdf)      |                                                                                                     |
| I | Villarreal, MacMillan   | Map the Impact                                                                                                                                                      | [link](#villarreal)  | [PPTX](../f20/clients/pitches/i.villarreal.pptx)  | [PDF](../f20/clients/villarreal.pdf)  |                                                                                                     |
| J | Vernon                  | Worldwide Venture Capital Competition Online Judging System                                                                                                         | [link](#vernon)      | -                                                 | [PDF](../f20/clients/vernon.pdf)      |                                                                                                     |
| K | Palmer, Millberger (1)  | ibiblio: reporting portal for radio stations                                                                                                                        | [link](#palmer1)     | [PDF](../f20/clients/pitches/k.palmer.pdf)        | [PDF](../f20/clients/palmer.pdf)      |                                                                                                     |
| L | Palmer, Millberger (2)  | ibiblio: billing portal for radio stations                                                                                                                          | [link](#palmer2)     | [PDF](../f20/clients/pitches/l.palmer.pdf)        | [PDF](../f20/clients/palmer2.pdf)     |                                                                                                     |
| M | Ward, Ward              | National Pet Obesity Survey App                                                                                                                                     | [link](#ward)        | -                                                 | [PDF](../f20/clients/ward.pdf)        |                                                                                                     |
| N | Steponaitis, Davis      | Reviving "Excavating Occaneechi Town"                                                                                                                               | [link](#steponaitis) | [PPTX](../f20/clients/pitches/n.steponaitis.pptx) | [PDF](../f20/clients/steponaitis.pdf) |                                                                                                     |
| O | Halpin                  | Peer learning in the time of COVID-19                                                                                                                               | [link](#halpin)      | [PDF](../f20/clients/pitches/q.halpin.pdf)        | [PDF](../f20/clients/halpin.pdf)      |                                                                                                     |
| R | Hurlbert                | Avian Diet Database                                                                                                                                                 | [link](#hurlbert)    | [PPTX](../f20/clients/pitches/r.hurlbert.pptx)    | [PDF](../f20/clients/hurlbert.pdf)    | [PPTX](../f20/clients/hurlbert.avian-diet-database-presentation.pptx)                               |
| S | Shah, Konz              | <sup>**IP**</sup> Gracefully                                                                                                                                        | [link](#shah)        | [YouTube](https://youtu.be/SkV-NTqOAkc)           | [PDF](../f20/clients/shah.pdf)        | [PPTX](../f20/clients/shah.presentation.pptx)                                                       |
| T | Scheier, Miller         | embit smartphone energy meter                                                                                                                                       | [link](#scheier)     | [PDF](../f20/clients/pitches/t.scheier.pdf)       | [PDF](../f20/clients/scheier.pdf)     |                                                                                                     |
| U | A. Jones, Watkins       | Building an Interface between User and Convolutional Neural Network-Segmented Cells and the Microscopist for Analytical  Descriptions of Subcellular Time and Space | [link](#a-jones)     | [PPTX](../f20/clients/pitches/u.a-jones.pptx)     | [PDF](../f20/clients/a-jones.pdf)     |                                                                                                     |
| V | McCormick, Weakley      | Dynamic map of Herbarium specimen use                                                                                                                               | [link](#mccormick)   | [PPTX](../f20/clients/pitches/v.mccormick.pptx)   | [PDF](../f20/clients/mccormick.pdf)   |                                                                                                     |
| W | Fisher, Sozzi, Stallard | <sup>**IP**</sup> Well-aware: a mobile app that helps private well users keep their drinking water safe                                                             | [link](#fisher)      | [PPTX](../f20/clients/pitches/w.fisher.pptx)      | [PDF](../f20/clients/fisher.pdf)      | [PPTX](../f20/clients/fisher.additional.pptx)                                                       |
| X | Morey                   | <sup>**IP**</sup> Charlie: The Citizen Journalist App                                                                                                               | [link](#morey)       | [PDF](../f20/clients/pitches/x.morey.pdf)         | [PDF](../f20/clients/morey.pdf)       |                                                                                                     |


<a name="r-jones"></a>

#### A. R. Jones, Katz, Cole - NC Therapeutic Riding Center Check-In App

We are looking to develop a check-in app for our staff, clients, and volunteers.
This app will need to work on an iPad and record various data points.

<a name="verber"></a>

#### B. Verber, Dick - Sweepstat App

We are building on an existing Android application framework to communicate with
an electrochemical device (the SweepStat) via Bluetooth with a user-friendly
GUI. This practical device and the Android app designed by our student team will
be used around the world for biosensing, material science, and educational
curricula.

<a name="taylor"></a>

#### C. Taylor - VR-Moria

Hooking up UMoria to OSVR to produce a VR interface to the classic dungeon crawler game.

<a name="vanderburg"></a>

#### D. Vanderburg - Connect

The purpose of this project is to develop an app that can help caregivers of
children with developmental disabilities connect so that they can learn from one
another, build relationships, and feel less alone while facing the unique
challenge of parenting a special needs child. Components will include
informational segments, a "dating app"-like component for parents to match
profiles, a chat forum, and organizational tools. Students in COMP 523 will have
the flexibility to choose one of these components to focus on this semester and
will have a say in the overall development and direction of the app.

<a name="blair"></a>

#### F. Blair - Software Project Analytics iOS App

No further details.

<a name="carter"></a>

#### G. Carter - ANTE

My name is Tyrell Carter; I am a senior from the University of North Carolina. I
am currently working on my startup ANTE which works to develop software to help
small businesses with efficiency. Right now, we are working on developing our
website and our software which will help our clients determine their pricing.

ANTE develops software to assist small businesses to run their businesses as
efficiently as possible. ANTE is currently apart of 2 UNC accelerator programs,
Launch Chapel Hill and UNC Campus Y’s Cube, and has already been awarded seed
funding.

Email: ante4sb@gmail.com

<a name="radsky"></a>

#### H. Radsky, Moon - Easy Access

Easy Access is an early stage social good start-up building college search and
advising tools to help low-income and first generation students access college.
The product that we are currently building is the counselor portal, a caseload
management tool for school counselors working with high school juniors and
seniors. The CS523 team would build upon existing product to add features
including: a payment system that allows different levels of access for different
counselors, refining data upload and download features, and enhancing the user
experience of existing pages.

<a name="villarreal"></a>

#### I. Villarreal, MacMillan - Map the Impact

We have multiple maps. They're great! They don't look alike or "talk" to each
other. They are scattered around our website. Help us draw our readers into a
fun, compelling look at public health at Gillings, be it a dashboard or another
integrative tool!

<a name="vernon"></a>

#### J. Vernon - Worldwide Venture Capital Competition Online Judging System

We're looking for an adaptable system to collect and display judges' votes for
events taking place around the world as part of the Venture Capital Investment
Competition. More at www.vcic.org.

<a name="palmer1"></a>

#### K. Palmer, Millberger 1 - ibiblio: reporting portal for radio stations

ibiblio online public library (https://www.ibiblio.org) makes available a wide
variety of free content. One category is streaming some not-for-profit radio
stations. This project is to update a web app for radio station managers to
obtain reports for their stations in a self-service manner rather than having to
request them and have an ibiblio admin manually generate them. The app is not
currently working due to Google API changes and PHP updates, and needs updated
for these changes and some enhancements.

<a name="palmer2"></a>

#### L. Palmer, Millberger 2 - ibiblio: billing portal for radio stations

ibiblio online public library (https://www.ibiblio.org) makes available a wide
variety of free content. One category is streaming some not-for-profit radio
stations. This project is to develop a web app to to automate the monthly
billing process for radio stations. The process is currently done by an ibiblio
admin manually copying data to a spreadsheet that does the calculation. Each
type of user (ibiblio admins, finance staff, radio station managers) will be
authorized to access different functionality in the app.

<a name="ward"></a>

#### M. Ward, Ward - National Pet Obesity Survey App

Pet obesity continues to be the number one health threat faced by US pets. The
Association for Pet Obesity (APOP) is the most recognized veterinary
organization dedicated to pet weight issues. APOP has been conducting national
pet obesity prevalence surveys with participating clinics (average 400)
since 2005. We have previously relied on written data collection sheets that are
then scanned, faxed, or mailed to us for analysis. We would like you to create
an app for veterinary professionals to use to enter basic pet exam data (name,
gender, breed, weight, body condition score (BCS), etc.) and, ideally, capture a
set of pet photographs from smartphones. An optional pet owner survey and
companion app with similar functionality would be immensely helpful. The app(s)
need to be able to export data as CSV or similar format for Excel or Google
Sheets for data analysis. If we succeed, your app will be used and seen by
thousands of veterinary professionals and pet parents. In addition, the team we
work with will be featured on all our social media channels (@DrErnieWard).
Thanks! Dr. Ernie Ward, Founder, Ocean Isle Beach, NC

<a name="steponaitis"></a>

#### N. Steponaitis, Davis - Reviving "Excavating Occaneechi Town"

In 1998, UNC Press published its very first electronic monograph, called
"Excavating Occaneechi Town" (EOT) as a CD-ROM. It brought together all the
research that had been done by UNC archaeologists at the Occaneechi Indian
village visited by John Lawson in 1701 (in present-day Hillsborough, NC) and
included an "Electronic Dig," which allowed the user to simulate an
archaeological excavation.

In 2003, this publication was moved to the Web. This version still exists (see
URL below), but is very clunky by modern standards and doesn't work well on
modern screens, whether on computers, tablets, or phones.

The project for this year's COMP 523 team would to update the entire EOT web
site with a new interface that uses up-to-date technology and is mobile-device
friendly. The site consists of thousands of flat HTML pages, integrated with
excavation maps, databases, and thousands of images. So, once the new interface
is designed, the challenge will be to automate the process of converting the old
HTML pages and images to the new ones without losing functionality. Given the
complexity of the site, this may be harder than it seems at first glance, but
should be doable.

[Brief background on the archaeological site](https://www.ncpedia.org/occaneechi-indians)

[Excavating Occaneechi Town (Web Edition, 2003)](https://rla.unc.edu/dig/)

<a name="halpin"></a>

#### O. Halpin - Peer learning in the time of COVID-19

Let’s support peer learning (in statistics) by designing some fun, interactive
web-based activities for students to work on together, without being co-located

<a name="hurlbert"></a>

#### R. Hurlbert - Avian Diet Database

The aim for this project is to build a mobile-friendly web page for interactive
data exploration and visualization of avian diets based on a large and growing
database being compiled in my lab. Users will be able to filter by bird species
or prey item and see quantitative summaries of all studies that have ever been
done and what the relative importance of different diet items is.

<a name="shah"></a>

#### S. Shah, Konz - Gracefully

We are a venture capital firm focused on improving the lives of senior citizens
through building sustainable businesses using technology. We believe that our
elders deserve to age gracefully with dignity and respect, and in modern
society, families are often too busy or live too far away to properly facilitate
care for seniors.

This project is to set up a web app platform Gracefully, to connect senior
citizens who need help with activities of daily living (such as housekeeping,
cooking, laundry, and just basic companionship), with prospective caregivers in
the region who are interested in providing such personal care services &
companionship. We aim to utilize our robust database of information submitted by
both caregivers and care seekers to better match the two sides. Through the
portal, users will be able to schedule care appointments, send messages and
manage payments. Consider it like a mix of Uber, TaskRabbit, and an online
dating app, focused on helping senior citizens!

“To care for those who once cared for us is one of the highest honors.”

<a name="scheier"></a>

#### T. Scheier, Miller - embit smartphone energy meter

embit is a smartphone energy meter that helps users power their phone from the
sun.

<a name="a-jones"></a>

#### U. A. Jones, Watkins - Building an Interface between User and Convolutional Neural Network-Segmented Cells and the Microscopist for Analytical  Descriptions of Subcellular Time and Space

There is a general need among a large number of microscopists for a tool to
analyze images of a cell to determine the distribution of proteins that have
been made fluorescent. This is a time consuming and laborious process that
begins by a human (the microscopist) determining the compartments of the cell
and regions in focus, then to acquire the fluorescence signal within the
compartments and do the calculations. We have used convutional neural network
(deep learning) to train a computer to do the laborious part saving the human
many hours of time. HOWEVER, we still need a user-friendly interface between the
CNN data and the output. The goal is to create software that allows the user to
select regions for analyses. The basic version of this interface absolutely can
be finished within the time of the semester by a COMP 523 team and if there is
more time left, we can add more features. We would like to publish this in a
peer-reviewed journal and so this is an opportunity for you to be a co-author.
For those of you who want to go to graduate school, a co-authored publication is
a "free ticket" for admission into the school of your choice.

<a name="mccormick"></a>

#### V. McCormick, Weakley - Dynamic map of Herbarium specimen use

I would like you to develop a way to visualize our botanical collections by
having a map that would show a ‘blip’ every time a specimen record is accessed
-- a botanist may be looking up rare plants of Tennessee and call up 3 or our
specimen records -- and blip blip blip on a map 3 red dots would appear. Ideally
the map would be a time-lapse movie showing record access over the past month or
past six months. I’d like to have the map running on our Herbarium website -- a
visual demonstration of our collections being used, and visual demonstration of
the worldwide scope of our collections.

<a name="fisher"></a>

#### W. Fisher, Sozzi, Stallard - Well-aware: a mobile app that helps private well users keep their drinking water safe

Well-aware: a free mobile application to help private well users keep their
drinking water safe

More than 10 million US households rely on private wells, and these wells are
vulnerable to contamination by lead, bacteria, viruses, and other pollutants.
Lead contributes to irreversible cognitive and developmental impairment in US
children, while diseases caused by microbial pathogens can stunt growth or even
be life threating in the most serious cases. Under the Safe Drinking Water Act,
piped water systems are regularly tested to make sure they are free from lead
and bacteria, but this act doesn cover private wells, and well users are
responsible for testing and managing their own water. Many well users don’t know
how or where to get their water tested, and some lack the resources to do it (a
test can cost $200 or more in some cases). Most have never tested their water
and don’t know whether or not it is safe. We propose to create a free mobile app
that helps users understand the risks to their well water quality, order and
learn to use effective, low-cost home test kits, automatically read and
interpret test result photos, and link users to local resources and guidance for
next steps, based on their results. The app will leverage off-the-shelf AI tools
to interpret photos taken by the user of their test results, taking some of the
guess work out of using these low-cost but sometimes challenging to interpret
low-cost test kits. As the software is in development, our existing student
field team will also be piloting the test kits with volunteer end users in two
NC counties with high numbers of private wells, and will be coordinating with
state and local health departments to maximize our impact. If successful, this
project has the potential to reduce risks for millions of well users in the US
and beyond. Additional details in [a recent
post](https://www.unc.edu/posts/2020/07/10/harvey-award-goes-to-autism-employer-support-mobile-app-for-well-water-testing/).

<a name="morey"></a>

#### X. Morey - Charlie: The Citizen Journalist App

City News Beat delivers Local News Weather and Lifestyle stories for Roku and
Fire TV users. Our software engine,"CHARLIE", powers our brands NYC News Beat,
Seattle News Beat, Bay Area News Beat, Carolina News Beat and Tar Heel News Beat
for Roku and Fire TV. However we need mobile iOS and Android apps that do more
than just play our content city by city.

Our newsroom tracks trending topics using Data based tools along with Google
Trends, Trends Map from Twitter and Snap's heat map. When we see a trending
story happening, we want to enable local citizens to submit video of that event
to us. To do so, we need to start with a web based tool for Story Assignments.
Something as simple as placing a pin drop on a map to find the location of
phones with our app loaded nearest to that event. W will then need to send paid
requests to those users within that radius of a breaking news event.

When we use a citizens video, we'll need a way to pay that user directly, so
user profile data collection is important. We have not yet decided on the off
the shelf electronic payment API - something that is easy and built into the web
tool (we are open to ideas here.)

That web tool is for our Newsroom Producer to create the story assignment (the
pin drop), send it to all phones in the radius or just to certain users (we deal
with Pro stringers as well), track submissions, mark the video we decide to use,
then pay the citizen shooter who sent it to us. Any video submitted to CNB, is
for City News Beat's exclusive use so the app cannot send it anywhere else (i.e.
Facebook et al).

To submit video to our newsroom we’ll need to capture info, we'll need our Terms
and Conditions accepted and we must have a way to connect two way payments with
our users.

The apps themselves must set the phone's camera to 4K (or HD mode at a minimum),
set lighting to Auto and it must turn and lock the screen into Landscape 16x9
mode for TV replay. On screen guides are necessary to help citizens "Frame The
Shot" properly - a simple [ + ] onscreen guide with a start and stop record
button. Recording should be done into the cloud, with a review on the phone. Any
video scrubbed by user must be deleted from our cloud account. When submitted to
us, an alert on our web tool that it has arrived with a thumbnail and the name
of the `user_Assignment_video-version-number` as the name of the file.

For users who just want the news, we want to enable our apps to stream content
to the app, similar to what we do already on our Fire TV and Roku version. They
will need to select a city and CHARLIE will do the rest.



