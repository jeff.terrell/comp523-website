---
title: Fall '20 Lecture Notes - Fri Aug 14 - Client Pitch Day 1 of 2
stem: f20.2020-08-14.client-pitch-day-1
---

- slides from the pitches are available on the [projects page](../projects/)
- video [on YouTube](https://youtu.be/26vVWlWA2rs)
- [transcript of video](../f20/lectures/2020-08-14.client-pitch-day-1/transcript.vtt)
