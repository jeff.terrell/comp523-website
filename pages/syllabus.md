---
title: Syllabus for COMP 523: Software Engineering Laboratory
stem: syllabus
---

### Contents

- [Bulletin description](#bulletin-description)
- [General course info](#general-course-info)
- [Instructors](#instructors)
- [Context and target audience](#context-and-target-audience)
- [Course description](#course-description)
- [Meetings outside of class](#meetings-outside-of-class)
- [Textbook and resources](#textbook-and-resources)
- [Prerequisites](#prerequisites)
- [Learning objectives](#learning-objectives)
- [Assignments](#assignments)
- [Talks](#talks)
- [Grading criteria](#grading-criteria)
- [Attendance](#attendance)
- [Lecture topics](#lecture-topics)
- [Deadlines and key dates](#deadlines-and-key-dates)
- [Classroom policies](#classroom-policies)
- [Honor code](#honor-code)
- [Mask Use](#mask-use)
- [Accessibility Resources and Services (ARS)](#accessibility-resources-and-services-ars)
- [Dealing with stress (CAPS)](#dealing-with-stress-caps)
- [Title IX Resources](#title-ix-resources)
- [Disclaimer](#disclaimer)
- [Gen-Ed flags](#gen-ed-flags)

### Bulletin description

Organization and scheduling of software engineering projects, structured
programming, and design. Each team designs, codes, and debugs program components
and synthesizes them into a tested, documented program product.

### General course info

- Term: Fall 2021
- Department: Computer Science (COMP)
- Course number: 523: Software Engineering Laboratory
- Section number: 001 (only section)
- Credit hours: 4
- Time: MWF 1:25pm - 2:40pm
- Location: Sitterson Hall, Fred Brooks Annex, room FB 009
- Website: [https://comp523.cs.unc.edu](https://comp523.cs.unc.edu)

### Instructors

#### Instructor: Dr. Jeff Terrell

- Email: [terrell@cs.unc.edu](mailto:terrell@cs.unc.edu)
- Office: Sitterson SN205
- Office hours: For app-related questions, visit the [UNC App
  Lab](https://applab.unc.edu) first. For class-related questions or
  second-stage technical questions, I will hold weekly office hours from
  10am-12pm on Tuesdays; you can [sign up for a 30-minute slot
  here](https://calendly.com/jeff-terrell/office-hours). I will also try to be
  responsive to email.
- Web: [http://terrell.web.unc.edu/](http://terrell.web.unc.edu/)

#### Teaching Assistant: Vraj Patel

- Email: [patelvap@live.unc.edu](mailto:patelvap@live.unc.edu)

### Context and target audience

This course is a bit unusual in the department. Most computer science courses
emphasize certain concepts, and some use programming assignments and projects to
reinforce and apply the conceptual learning; the focus is the concepts. This
course assumes familiarity with a breadth of computer science concepts and
emphasizes the _application_ of those concepts. It is primarily about _building
software_ that will be used by others. Although there is some conceptual
content, most of it is in direct service to the process of building software.

Another unusual aspect of this course is the emphasis on collaboration. Other
courses wisely proscribe or limit collaboration because your colleagues cannot
learn for you, and the individual learning is essential. COMP 523, on the other
hand, requires collaboration. Your project is not an individual but a team
effort. Thus, you will learn the difficulty&mdash;and hopefully also the
joy&mdash;of effective collaboration.

Because you will be applying concepts learned across a variety of other computer
science courses, this course has many prerequisites and is typically taken by
senior (and sometimes junior) computer science majors. Majors intending to
pursue a career in research or academia are encouraged to take this course
because it's rare that the career of a CS major will not involve code at some
level, and this course provides probably the most thorough exposure to practical
software engineering available in the major, apart perhaps from an internship.
For the same reason, CS majors intending to pursue a career in software
engineering are especially encouraged to take this course.

### Course description

The goal of this course is to teach and provide experience building software
projects in service to somebody who needs it, as part of a small team. Most of
the work you will do in this course is on the project, and the project's grade
is weighted accordingly.

The projects in this course are motivated by the software needs of people
outside the classroom. The instructor finds these clients before the semester
starts, hears about their needs, and assesses how well each potential project
fits with the objectives and constraints of the course, including the size of
the project and skills needed by students. Approved clients pitch their
projects at the beginning of the semester, and teams (of 3&ndash;4 students)
indicate their preferences to inform a match-making process. Once project
assignments are decided, each team meets with its client to understand the
requirements and priorities of the client, which starts the software
development process, a process that continues until the end of the semester.

Along the way, teams meet weekly with their assigned mentor, who might be a
volunteer from industry, the instructor, or a TA. In these meetings, mentors
provide feedback intended to enrich teams and avoid problems and help solve any
problems that have surfaced or are anticipated. They also review deliverables
and demos and document progress to support the instructor in grading your
efforts. This weekly cadence is also important to counter the natural tendency
to procrastinate on the project, which is an unrewarding experience for
students and clients. Note: if you have problems with your mentor, you should
contact me.

This course is a CI (communications intensive) course. The meetings with your
client and your mentor involve extensive communication. Also, the ongoing
project work involves frequent coordination with your team to assign tasks and
communicate questions, issues, and completions. In addition, about 20% of your
grade depends on three talks that you deliver to the class as part of your
team.

When clients and teams are not presenting, the instructor will lecture. Most
lectures are of a more general nature and intended to help teams work on their
projects more effectively, such as agile project management. There may also be
occasional guest lectures from professionals with relevant experience.

The course also carries an EE (experiential education) designation and is an
APPLES service-learning course.

### Meetings outside of class

In keeping with the importance on communications and the CI designation, this
course involves many meetings. Besides the class meetings, students are expected
to:

- meet weekly as a team
- meet weekly with their mentor
- meet with their client as requested (I recommend to the client to meet every
  other week), although not necessarily with the whole team

Scheduling meetings involving so many busy people can be a challenge! To help
with this, you have a one time that is already guaranteed to work by virtue of
the fact that you're successfully registered for this course. This class
technically meets on Monday, Wednesday, and Friday of each week. Apart from the
beginning the semester, there will be no lectures on Friday, so you are
guaranteed to have that time available--unless you fill it up with something
else, and please, for the sake of your team, don't do that.

That's 3 types of meetings and 1 reserved time slot. You should prioritize the
team meetings and mentor meetings above the client meetings, because your whole
team must be present for those, and because they are more directly relevant to
your grade. That said, if your client wants to meet with your team, do your
best to find a time that at least one or two team members can meet.

### Textbook and resources

There is one required textbook: _Debugging: The 9 Indispensable Rules for
Finding Even the Most Elusive Software and Hardware Problems_ by David J. Agans.
I expect that its rules will come in handy as you work on your projects. The
[calendar](../calendar/) lists expectations of by when certain chapters should
be read. After these dates, you can expect to see the content of those chapters
in quizzes.

There may occasionally be recommended readings or other resources; if so, they
will be available for free online and linked from the class webpage. Such
content will not be on quizzes unless otherwise noted.

After gaining experience with software engineering from this course, you may
enjoy reading _The Mythical Man Month_ by department founder Dr. Fred Brooks.
It's a classic for a reason.

### Prerequisites

- COMP 401 (Foundation of Programming)
- COMP 410 (Data Structures)
- COMP 411 (Computer Organization)
- At least two of:
    - COMP 421 (Files and Databases)
    - COMP 426 (Modern Web Programming)
    - COMP 431 (Internet Services and Protocols)
    - COMP 433 (Mobile Computing Systems)
    - COMP 520 (Compilers)
    - COMP 530 (Operating Systems)
    - COMP 535 (Introduction to Computer Security)
    - COMP 560 (Artificial Intelligence)
    - COMP 562 (Introduction to Machine Learning)
    - COMP 575 (Introduction to Computer Graphics)
    - COMP 580 (Enabling Technologies)
    - COMP 585 (Serious Games)

The most useful courses to have taken before this one are COMP 421 and COMP 426.
These concepts are typically needed in almost every project.

### Learning objectives

At the end of the course, each student will have experienced all aspects of a
software development project, including:

- working with a client to define goals and priorities
- designing a system
- scheduling and planning a multi-person project
- effective communications
- running meetings
- writing technical documentation
- preparing web content
- writing and testing code
- deploying the system
- public presentations

The course also conveys several Gen-Ed flags (see [a later
section](#gen-ed-flags) for justifications):

#### Creative Expression, Practice, and Production

1. Compose, design, build, present, or perform a work that is the result of
   immersion in a creative process using appropriate media, tools, and
   techniques.
2. Explain the roles and influences of creativity, technologies, materials, and
   design processes in the creation of knowledge, expression, and effective
   solutions.
3. Evaluate their own and others' creative work to demonstrate how critique
   creates value in creative domains.

#### High Impact Experience

1. Explain the connections between academic studies and outside-the-classroom
   experiences and observations.
2. Apply knowledge in complex or ambiguous situations.
3. Develop questions from experiences and observations to deepen and extend
   academic inquiry

#### Communication Beyond Carolina

1. Ascertain the expectations, opportunities, and barriers to oral
   communication in distinct situations.
2. Tailor communications to different kinds of settings, including individual,
   small group, and public communication.
3. Tailor communications to different levels of expertise (inexpert, informed,
   expert), and to varying levels of alignment (resistant, ambivalent,
   supportive) and distinct contexts.
4. Make informed situation- and audience-sensitive strategic choices in content
   and delivery.
5. Improve ability to move audiences, as measure by best practices, audience
   feedback, and instructor feedback.

#### Research and Discovery

1. Frame a topic, develop an original research question or creative goal, and
   establish a point of view, creative approach, or hypothesis.
2. Obtain a procedural understanding of how conclusions can be reached in a
   field and gather appropriate evidence.
3. Evaluate the quality of the arguments and/or evidence in support of the
   emerging product.
4. Communicate findings in a clear and compelling ways.
5. Critique and identify the limits of the conclusions of the project and
   generate ideas for future work.

### Assignments

In the course of developing a project, your team will deliver:

- a project management board
- a web site describing the project and containing other assignments
- a list of prioritized project requirements as &ldquo;user stories&rdquo;
  (from consultation with the client)
- a clickable prototype (i.e. a linked set of prospective screen images)
- a decision and justification of the technology platform
- an architecture diagram
- a &ldquo;walking skeleton&rdquo;; that is, a bare-bones deployment
  demonstrating end-to-end functionality
- a test coverage report
- a developer-oriented README
- a client-oriented project overview
- two brief service-learning reflections

Also, your team will present three talks to the class. See the next section for
details.

Finally, you will personally deliver a report on the project from your
perspective, including an assessment of how each of your teammates performed.

More information is available on the [assignments page](/assignments/) of the
web site.

### Talks

Your team will deliver three talks to the class.

1. A brief (~5 minute) midterm presentation describing your project and the
   progress to date, including UI designs, the platform selection, and a walking
   skeleton demo, which will be given about halfway through the semester.
2. A longer (~20 minute) tech talk teaching some tool or technology to the
   class. This is a great way to present and learn about new technologies. This
   talk will be given towards the end of the semester.
3. A final presentation (of about 9 minutes) including the project status and a
   retrospective about lessons learned, which will be given during the final
   exam period for this course.

More details about the talks will be available on the project website.

### Grading criteria

#### Overall grade

- 10% is for attendance and participation in class meetings and mentor meetings
    - 5% for participation in class (in-person or virtual)
    - 5% for on-time attendance at mentor meetings
- 10% is for quizzes
- 15% is for the tech talk
- 65% is for the project
    - 32.5% for the assignment grade
    - 32.5% for the app grade
    - with an individual multiplier for personal effort

#### Attendance and participation

During class, I will occasionally have participatory polls and exercises,
typically via PollEverywhere. I will collect responses, and your response
(whether it is correct or not) serves to demonstrate your attendance and
participation in class. Each class period will be have an equal weight in the
final grade. Note that some polls will be at the beginning or end of class, so
if you arrive late or leave early you may miss them.

Mentors will report to me who was present and on-time for mentor meetings. If
you are late but present, you will get half credit for the meeting.

#### Quizzes

To encourage your attention during lectures, even when the material isn't
immediately relevant, I will give a series of quizzes for lecture content. My
intention is not to require your extensive study, since the other course
requirements are substantial. However, a complete lack of accountability for
lecture content has, in my opinion, been a disservice to my students in a
previous iteration of the course. Even content that isn't useful to a presently
felt need often finds utility in the future.

My current plan is to have a 10-minute quiz with about 10 multiple choice
questions on it at the end of class about every other week, as indicated on
[the calendar](../calendar/). (I may adjust this plan as the situation and
student feedback warrant, but students will be given notice about any changes.)
My aim is that those (and only those) who attended to lectures will be able to
get good scores without studying.

#### Project grade

There are two components to the project grade, each worth half: the assignment
grade and the app grade.

##### Assignment grade

Each [assignment](../assignments/) is graded on a 2 point scale. Points are
awarded based on what proportion of the requirements were met on time; for
example, an assignment meeting all requirements will earn 2 points, and an
assignment missing about half of the requirements will earn 1 points. Only whole
numbers will be used for grading, to avoid needlessly fine distinctions. In the
event that the assignment is of exceptional quality, a third point may be
awarded. I expect that this will be uncommon; after all, not many things can be
exceptional.

After [Assignment 8: Walking Skeleton](../walking-skeleton/), [demo
videos](../demo-videos/) will be due each week at the usual weekly deadline.
These are short videos demonstrating a feature that has been developed and is
now ready for client review. These videos will be graded on the same 2 point
scale. A video earning 2 points clearly demonstrates the feature in a way that
the client can reproduce.

Each week will have equal weight in determining the assignment grade. So when
two assignments are due in one week, or when an assignment and a demo video are
due, the individual scores are averaged to compute the assignment grade for the
week. The total assignment grade is the average of all the weeks.

Assignments 0 ([project preferences](../project-preferences/)) and 15 ([personal
report](../personal-report/)) are not graded and do not contribute to the
assignment grade. Assignment 10 ([tech talk](../tech-talk/)) is graded
separately and does not contribute to the assignment grade.

I do not expect teams to get assignments and demo videos perfect the first time,
with no revisions needed. I do expect completion by the due date, which will
earn full credit. **Meeting all assignment requirements on time is the easiest
way to improve your grade in this course.**

If your assignment or your demo video earns less than 2 points, you will have a
list of things to add or change. If you accomplish everything on the list by the
grading period for the next week, you will get half of the lost points back.

##### App grade

The app grade is assessed after the final presentation. Because the apps are all
different, there is no standard grading rubric, and grading is inherently
subjective. However, the following qualitative criteria should help clarify my
expectations and provide some consistency.

An app getting a grade of A:

- is well designed and attractive;
- has an intuitive user interface;
- is deployed;
- includes at least the most important features;
- has good test coverage (probably over 70%, although that can depend on the
  project), and
- is free of obvious bugs and errors.

An app getting a grade of B might miss 2 of these criteria, or else do a
mediocre job at many of them.

An app getting a grade of C has significant problems.

#### Individual grade

Once projects are assigned grades, your personal contributions are weighed
(based on your mentor's observations as well as the personal report turned in
by your teammates), and I multiply the project grade by a individual effort
factor that is typically between 0.7 and 1.1, although could be as low as 0.5
in extreme cases. It will rarely be above 1; if you are contributing
significantly more than the other members of your team, that is often more
indicative of team dysfunction than individual heroism. In order to get a 1,
you must:

- meet all role responsibilities
- attend and participate in all meetings
- commit to a reasonable amount of work
- deliver on your commitments
- help team members when appropriate
- not preempt work from others nor denigrate their contribution

#### Grading scale

- 95.0 &le; A
- 90.0 &le; A- &lt; 95.0
- 87.0 &le; B+ &lt; 90.0
- 83.0 &le; B  &lt; 87.0
- 80.0 &le; B- &lt; 83.0
- 77.0 &le; C+ &lt; 80.0
- 73.0 &le; C  &lt; 77.0
- 70.0 &le; C- &lt; 73.0
- 67.0 &le; D+ &lt; 70.0
- 60.0 &le; D  &lt; 67.0
- F  &lt; 60.0

### Attendance

Attendance is required, and in-class participation accounts for 5% of your
overall grade.

However, per the UNC community guidelines, you should not attend class in person
if you have any symptoms of COVID-19. So there will also be a virtual option on
Zoom. If you attend virtually, you must still participate to receive credit for
attending.

If I develop symptoms of COVID-19, I will either hold class remotely via Zoom or
else record a lecture. In either case, I will email everybody as plans develop.

If an illness, family emergency, or other university-recognized reason for
missing lecture occurs, you can provide an official letter from the Office of
the Dean of Students recognizing the excuse, in which case you will not be
penalized for the absence.

### Lecture topics

In approximate order:

- course overview
- software project overview
- working with clients
- project management
- design principles
- Figma workshop
- user stories
- evaluating technologies
- layout
- client/server architecture
- infrastructure
- git
- interpersonal effectiveness
- testing
- agility
- pair programming
- authentication

### Deadlines and key dates

See the course website for details about each of the
[assignments](/assignments/), including due dates. Other important dates are:

- Wed Aug 18: first day of class
- Mon Aug 23 and Wed Aug 25: client pitch days
- Mon Sep 6: Labor Day (no class)
- Wed-Fri Nov 24-26: Thanksgiving holiday
- Wed Dec 1: last day of class
- Sat Dec 4: final exam (12-3pm)

### Zoom policies

I will attempt to keep an eye on the participants list and chat window of Zoom
while lecturing. If you have a question, raise your virtual hand or ask your
question in the chat window, and I'll get to it soon. Please refrain from
general chatting in the Zoom chat window, the same way you would (hopefully)
refrain from chatting in a physical classroom during lecture. However, please
feel free to chat before and after lecture.

### Honor code

Collaboration is an essential part of almost every graded activity in this
course. However, for in-class quizzes, collaboration will be considered
cheating. Also, reports on members of your team at the end of the semester
should of course be individual efforts.

### Mask use

This semester, while we are in the midst of a global pandemic, all enrolled
students are required to wear a mask covering your mouth and nose at all times
in our classroom. This requirement is to protect our educational community—your
classmates and me—as we learn together. If you choose not to wear a mask, or
wear it improperly, I will ask you to leave immediately, and I will submit a
report to the Office of Student Conduct. At that point you will be disenrolled
from this course for the protection of our educational community. Students who
have an authorized accommodation from Accessibility Resources and Service have
an exception. For additional information, see Carolina Together.

### Accessibility Resources and Services (ARS)

The University of North Carolina at Chapel Hill facilitates the implementation
of reasonable accommodations, including resources and services, for students
with disabilities, chronic medical conditions, a temporary disability or
pregnancy complications resulting in barriers to fully accessing University
courses, programs and activities.

Accommodations are determined through the Office of Accessibility Resources and
Service (ARS) for individuals with documented qualifying disabilities in
accordance with applicable state and federal laws. See [the ARS
website](https://ars.unc.edu) for contact information or email ars@unc.edu.

### Dealing with stress (CAPS)

Please be aware that this course can be stressful. One of the key stressors
stems from collaboration: the choices and contributions of your teammates are
out of your control, yet they affect you. Furthermore, you may experience more
interpersonal conflict in this course than in any other course you've taken at
Carolina. Lastly, you may feel a heavier weight of responsibility in this class
than others, because if you fail, you are disappointing not only yourself but
also your teammates and your client.

Some people are better equipped to tolerate and navigate such stresses than
others. If your stress or anxiety are interfering with your sleep or your life
in an unusual way, please consider getting help. There's no shame in that.
Indeed, I have been the grateful recipient of such help in the past.

CAPS is strongly committed to addressing the mental health needs of a diverse
student body through timely access to consultation and connection to clinically
appropriate services, whether for short or long-term needs. Go to [the CAPS
website](https://caps.unc.edu/) or visit their facilities on the third floor of
the Campus Health Services building for a walk-in evaluation to learn more.

### Title IX Resources

Any student who is impacted by discrimination, harassment, interpersonal
(relationship) violence, sexual violence, sexual exploitation, or stalking is
encouraged to seek resources on campus or in the community. Please contact the
Director of Title IX Compliance (Adrienne Allison – Adrienne.allison@unc.edu),
Report and Response Coordinators in the Equal Opportunity and Compliance Office
(reportandresponse@unc.edu), Counseling and Psychological Services
(confidential), or the Gender Violence Services Coordinators (gvsc@unc.edu;
confidential) to discuss your specific needs. Additional resources are
available at safe.unc.edu.

### Disclaimer

The professor reserves the right to make changes to this syllabus, including
assignment and project due dates, as well as percentages for assignments and
exams towards final course grade. These changes will be announced as early as
possible and will be reflected on [the course
website](https://comp523.cs.unc.edu). If there are discrepancies between this
syllabus and the website, the website is considered the authoritative resource.

### Gen-Ed flags

This course carries a number of gen-ed flags at UNC. A justification for each
follows. These flags also imply certain learning objectives, which are included
[above](#learning-objectives).

#### Creative Expression, Practice, and Production

##### Catalog description

Students engage in individual and collaborative creative expression,
exploration, or production, such as in performance, visual art, composition,
design, or technology. They engage with tools, techniques, methods, design
processes, technologies, and materials for creating works that express,
innovate, or create solutions to problems.

##### Questions for students (from the catalog)

1. What processes and practices can I use to produce meaningful expression or
   effective solutions with lasting impact?
2. How does collaboration and teamwork change or enhance the creative process?
3. How does a design strategy affect or enhance the creation and evaluation of
   a work of value?

##### Justification

The major output of this course is a created software product. Students learn
and use tools, techniques, methods, design processes, technologies, and
materials (e.g. software libraries) to create it. They are solving a problem
brought to them by a client outside the class.

#### High Impact Experience

##### Catalog Description

Students enrich and expand their academic study by engaging in compelling
applied experiences that transform their learning.

##### Questions for students

1. How do things I've learned in the classroom apply to outside settings?
2. How can experiences and observation raise or answer questions in academic
   settings?
3. How can I meaningfully reflect to help navigate complexities and ambiguities
   I encounter?

##### Justification

This course applies many concepts taught in other courses to computer science
majors. Much of the learning that occurs in this course happens in the context
of the project, not the lectures.

#### Communication Beyond Carolina

##### Catalog Description

Students build capacities for producing and listening to oral communication
across a range of contexts. With multiple audiences, they learn to listen to
and persuasively convey knowledge, ideas, and information.

##### Questions for students

1. How can I engage with audiences through oral communication?
2. How do I best convey knowledge, ideas, and information effectively to
   different audiences in situations?
3. How can I best understand the views and ideas of others, both individually
   and collectively?
4. What are the best ways of strategizing and delivering oral communication for
   achieving my intended outcomes?
5. How can media or digital compositions extend my ability to communicate?

##### Justification

In this course, students regularly discuss their project with:

- the other members of their team,
- a mentor with software development experience, and
- a client who typically does not have software development experience.

Also, student teams present to the class, and each team member must participate
in the presentation. There are three presentations:

1. a mid-term progress report
2. a longer technical talk
3. a final presentation

Furthermore, each team must add various reports to a project web site that they
create.

Thus, oral communication is a pronounced theme.

#### Research and Discovery

##### Catalog Description

Student immerse themselves in a research project and experience the reflection
and revision involved in producing and disseminating original scholarship or
creative works.

##### Questions for students

1. How do I establish my point of view, take intellectual risks, and begin
   producing original scholarship or creative works?
2. How do I narrow my topic, critique current scholarship, and gather evidence
   in systematic and responsible ways?
3. How do I evaluate my findings and communicate my conclusions?

##### Justification

The major output of the course is a creative product. Although the idea is
brought to the course from a client external to the course, the students must
decide how to approach the problem, which tools to use, how to collaborate
effectively, etc. Students must reflect on their progress and methods in many
of the weekly assignments, and such reflection and refinement is a significant
part of the mentoring meetings that happen weekly.
