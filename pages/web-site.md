---
title: Assignment 1: Web Site
stem: web-site
---

### Due

Tuesday, September 7<sup>th</sup> at 8am (because Monday 9/6 is Labor Day)

### Context

Your team's web site should contain all the information that your mentor, the
TA, and I will need to understand your project, your plans, and your progress.
All of your team assignments will be available from your website. It will also
be a useful record for your client and for future COMP 523 students to learn
about your project. Thus, this assignment is about creating a vehicle for
various kinds of collaboration.

Furthermore, certain questions below are intended to help you establish some
ground rules for collaboration among your team. Anything you decide can change
later, so you're not locked in. But it can be useful to have these conversations
early to establish shared expectations among the team.

### Requirements

There are two types of requirements: content and structure. Additionally, the
site must be published somewhere. When it is finished, email the URL to me at
[terrell@cs.unc.edu](terrell@cs.unc.edu) and I will post it to the class
website.

#### Content requirements

Your web site should include the following information:

- the title of your project
- a tweet-sized project summary (280 characters max)
- a longer project description (~1 page max; target audience: your mom),
  describing:

    - the need for the software
    - the target software platform (e.g. web app, mobile app, etc.)
    - the target users of the software
    - who the client is
    - why we should care about it
    - what the primary goals of the project are

- the names and email addresses of team members, including an "Email us" link
  with a URL like
  `mailto:user1@unc.edu,user2@unc.edu,user3@unc.edu,user4@unc.edu`
- the team roles, which might include roles like client manager, project
  manager, webmaster, tech lead, design lead, backend lead, frontend lead,
  infrastructure lead, etc. (Again, these can change later.) One person can have
  multiple roles. It's assumed that everybody will be coding to some extent.
- your team rules, including:

    - team behavior: Should a team member who will miss a meeting notify other
      team members in advance? If so, who, and how far in advance? Should team
      members be expected to be in frequent communication with each other? If
      so, how frequent, using what communication medium (email, group chat,
      etc), and how long a delay is OK (maybe distinguishing weekdays from
      weekends)? What happens if there is no response from somebody, and others
      are waiting on the work? You do not have to answer all of these questions,
      nor must you limit yourself to only these questions. Figure out what makes
      sense for your team.
    - coding style: Is there a particular style guide that you'd like to follow
      for your code? Many collaborative software projects require a consistent
      style because it is easier to read and it avoids superficial changes. But
      some teams will prefer not to constrain how each person codes. Either way
      is fine; figure out what makes sense for your team. As an example, here
      is [a style guide with linter and automatic
      formatter](https://standardjs.com/) that many use for Javascript.
    - git collaboration: Is there a branching and merging style that your team
      prefers? Two relatively common approaches are [git
      flow](https://nvie.com/posts/a-successful-git-branching-model/) and
      [trunk-based
      development](https://www.endoflineblog.com/oneflow-a-git-branching-model-and-workflow).
      (If in doubt, I recommend the latter.) Are pull requests and code review
      required, optional, or not expected? You may also want to read [a set of
      git best practices I wrote](https://github.com/RoleModel/BestPractices/)
      for colleagues at my last job. Again, having a policy of each dev
      committing however they like is an acceptable choice.

- a journal, which documents meetings, decisions, discoveries, [demo
  videos](../demo-videos/), and anything else important in the progression of
  your project (and should be updated at least weekly during the semester)
- a schedule of meetings, including standing (default) meeting times for your
  team, your mentor, and your client
- the deliverables, containing a list of all finished deliverables, which at
  this point will only include a link to the project management approach you
  devised in [Assignment 2](../project-management-assignment/). Include the link
  even if your board is not public—but if it isn't, note that fact on the web
  site.

#### Structure requirements

Although I dislike constraining artistic expression, I want you to conform to
the following structure for the site. (I'm constraining you here for two
reasons: consistency among sites makes it easier for me and the TA to navigate,
and I suspect you have more important things to do than to craft a web site.)

- The title of your project should appear at the top of every page in a
  consistent fashion.
- A top-level navigation menu should appear below the title in a consistent
  location on every page. This could be as a horizontal nav bar across the page,
  like what's on this web site, or it could be a vertically oriented menu at the
  left or right side of each page.
- The navigation menu should have the following items, in the same order, and
  each item should link to a page with the following content:

    - Project, including the project summary and longer project description.
    - Team, including names, emails, and roles of team members, followed by
      team rules.
    - Journal, including the meeting schedule and the journal itself, which
      should be sorted in reverse time order (i.e. most recent first).
    - Deliverables, including a menu of links to deliverables, in numeric order
      (i.e. starting with Assignment 2). Note: the actual deliverables should
      be on separate pages; this page should merely link to the deliverables,
      not include the deliverable content.

- The title bar should also be a link to the landing page (a.k.a. the "home"
  page or the "index" page).
- The landing page should include an explanation of the purpose of your site:
  that it is part of this course (include a link to this course web site) and
  contains relevant information about the project.

### Deployment

Deploy your site wherever you like. Below are some ideas.

Most (maybe even all) projects in the fall 2019 semester used
[web.unc.edu](http://web.unc.edu/) to create and manage their site with
WordPress. As of fall 2020, web.unc.edu has been replaced with
[tarheels.live](https://tarheels.live/). Both of these options use
[Wordpress](https://wordpress.org/) as a content management system
([CMS](https://en.wikipedia.org/wiki/Content_management_system)).

I use [Netlify](https://www.netlify.com/) to host this website. It lets you
create a site as a folder, then deploy the directory to the web via git. A good
pairing with Netlify is an SSG, or static site generator, which compiles
content, typically written in
[Markdown](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet),
into a static web site in a directory. Popular ones include
[Jekyll](https://jekyllrb.com/), [Hexo](https://hexo.io/), and
[Hugo](https://gohugo.io/), but there are
[plenty](https://staticsitegenerators.net/) to choose from. I even wrote [my
own](https://gitlab.com/unc-app-lab/clj-ssg/) for this site and the [App Lab
site](https://applab.unc.edu/).

Lastly, [GitHub Pages](https://pages.github.com/) could be a good option,
although I haven't used it myself.
