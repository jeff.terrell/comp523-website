---
title: Assignment 7: Architecture Diagram
stem: architecture-diagram
---

### Due

Monday, October 4<sup>th</sup> at 8am

### Context

The architecture diagram describes the highest level external view of the
software system. It is an easy way for your mentor and your client to
understand the system at a high level, and it tends to be something that both
you and they (and me) refer back to regularly. You will also show this diagram
to the class during your midterm presentation and final presentation.

### Requirements

Create an architecture diagram and post it to your project web site. It will
typically have between 3 and 7 components. It should show and label the
high-level components of the system and the interactions between components.
Also name which major languages, tools, or technologies are used for each
component, and include logos where possible.

If there are different major parts of the frontend, e.g. an administrative
section that only administrators can use, include that on the diagram.

Include a user icon to indicate how users interact with the system. If there
are different kinds of users, include an icon for each.

### Hints

Powerpoint or Google Slides can be fine ways to create your diagram. Figma
works too.

I discussed architecture diagrams and gave some examples in [the lecture on
Sept. 13](../f21.2021-09-13.architecture-simplicity/). In particular, I gave
the following tips for creating architecture diagrams:

1. include a user icon, or several if there are different roles/classes of users
1. use arrows and label them, with verbs or nouns, to explain the interaction
1. put related things near each other (see design rule P: proximity)
1. feel free to group related things together with a label where it makes sense
1. indicate when there is a single resource or multiple resources (e.g. multiple pages/screens, but only 1 database)
1. indicate where code runs/where services live (e.g. browser, mobile device, the cloud)

You can also see examples by looking at the websites of projects from previous
semesters ([F '19](../2019-projects/), [F '20](../2020-projects/)).
