---
title: Fall '20 Lecture Notes - Mon Aug 10 - Welcome
stem: f20.2020-08-10.welcome
---

- slides in [self-contained HTML](../f20/lectures/2020-08-10.welcome/slides.html)
- slides in [PDF](../f20/lectures/2020-08-10.welcome/slides.pdf)
- video [on YouTube](https://youtu.be/MWX7TCYg3_k)
- [transcript of video](../f20/lectures/2020-08-10.welcome/transcript.vtt)

