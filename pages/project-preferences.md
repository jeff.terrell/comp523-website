---
title: Assignment 0: Project preferences
stem: project-preferences
---

### Due

Friday, August 27<sup>th</sup> **by 8:00am**

I will endeavor to finish matchmaking by the start of class Friday; in any case,
I will post matches to the website as soon as possible.

### Context

After watching the client pitches and reviewing the [project
details](/projects/) as necessary, you and your team need to decide what
projects you prefer. I will use these preferences to assign teams to projects.
Once assignments are made, you and your client are stuck with each other for
the semester!

Some of you may have a stronger preference about who you are teamed up with
than you do about the client or project. You will have the opportunity to
indicate that.

This semester, 7 out of 30 projects invited to pitch have concerns about
intellectual property or confidential information. For those projects, students
will be asked to sign some forms agreeing to confidentiality and assignment of
intellectual property. (This tends to be more important for companies than
individuals.) You will have the opportunity to indicate whether your entire team
is comfortable with this, or not, and you will not be penalized in any way for
saying you're not comfortable. If you have questions about any of this, feel
free to contact Peter Liao of the UNC Office of Technology Commercialization
(who spoke briefly on this topic on the second day of class) at
[liaopb@unc.edu](mailto:liaopb@unc.edu).

### Requirements

First, indicate whether you prefer to prioritize your teammate preferences or
your project preferences. I will do my best to take all of your preferences,
and especially this one, into account. In either case, you may (and are
encouraged to) include both sets of preferences.

To indicate your preferences, your team should collectively send an email to me
at [terrell@cs.unc.edu](terrell@cs.unc.edu). (That is, send one email per team,
not per person.) For now, it's OK if you're a team of 1. The email should:

- be sent from one team member;
- copy the other team members, if any (max: 2<sup>\*</sup>);
- include a list of 10 letters, which indicate your project preferences in order
  (for example, "A B C D E F G H I J" would indicate that project A is your
  first preference, project B is your second, etc.);
- mention any important constraints (e.g. 2 of 4 team members do not have Macs
  so cannot easily develop an iOS app) or strong preferences not to work on
  certain projects (which I will do my best to accommodate but cannot
  guarantee); and
- indicate whether you are comfortable with an IP assignment and
  confidentiality agreement
- include the names of your preferred teammates (max: 2<sup>\*</sup>).

Here is a template for your email. Substitute anything in [brackets] with your
answer.

```
I/we prefer to prioritize the [team/project] preferences.

My/our project preferences are: [list of 10 letters]

Am I/Are we comfortable with an IP assignment and confidentiality agreement? [yes or no]

My/our preferred teammates are: ["none" or list of 1-2 names]
```

<sup>\*</sup> Some of you already have a team of 4 in mind. You _may_ submit the
4-person team, but please be aware that, depending on how the numbers work out,
I may break up the team.

**Please don't drop the class after submitting this assignment!**

### Grade

There is no grade for this. However, if you do not submit your preferences (or
submit them late), I will pick a project for you.

### Hints

There are many ways to rate project ideas. Here are some of them. Different
people weigh these aspects differently. It might be worthwhile to discuss with
your team, if you have one, what the most important aspects are to each of you.

- clarity of the proposal
- maturity of the idea (e.g. are there UI designs already?)
- client flexibility (if you can't deliver all that they want or discover that
  what they are asking for isn't very feasible from a technology perspective,
  will they roll with the punches and still be easy to work with?)
- social impact of the project
- connections of the client (e.g. can the client hire you after graduation if
  they like your work?)
- impressiveness of the project for your portfolio and interviews
- what languages, tools, and technologies might you be able to use or learn?
- client friendliness
- client availability (especially important for busy teams)
