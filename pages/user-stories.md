---
title: Assignment 3: User Stories
stem: user-stories
---

### Due

Sunday, September 13<sup>th</sup> at 8am

### Context

To start on your project, you have to first understand what you're building. At
the start of this process, the vision of the product is entirely in the head of
your client and any artifacts they have produced (e.g. the application they
submitted to become a client for the class). Your job is to get these ideas out
of their head and in a list for the team that is concise yet precise. Not only
that, but you need to understand which features are essential and which are
optional.

This list will serve as the basis for your list of project tasks to work on
during the semester.

This list is important not just to seed your project management board, if you
have one, but also to document the initial understanding of features, so that,
if things change drastically, you have a reference by which to assess the
degree of change (and perhaps justify to others why you didn't get as much done
on your project as others, because of changes in the client's desiderata).

### Requirements

Your requirements should be written in the form of &ldquo;user stories&rdquo;.
Each user story contains:

1. a **role**, starting with &ldquo;as a&rdquo; to indicate whose perspective is
   being adopted;
2. a **benefit statement**, starting with &ldquo;in order to&rdquo;, to describe
   the motivation for the feature; and
3. a **capability**, starting with &ldquo;I can&rdquo;, to describe what the
   feature accomplishes.

A few examples follow.

> As an authenticated user, in order to keep up with my friends, I can see a
> list of recent posts from my friends, most recent first.

> As the product owner, in order to track growth of the app, I can log in to an
> analytics dashboard to see how many users have been using the app recently.

> As a logged out user with an account, in order to use the app in a
> personalized way, I can type my email address and password to log in to the
> app.

Notice that the user stories can be a bit vague. For example, there are no
details about where things are displayed on the screen, or what the button
labels might be. That's OK. User stories are _conversation starters_, not a
water-tight contract.

For this assignment, work with your client to capture user stories for all of
the features that he or she envisions. Also, work with your client to
categorize the user stories into &ldquo;need-to-have&rdquo; features versus
&ldquo;nice-to-have&rdquo; features.

I expect that most projects will have between 6 and 20 user stories to capture
the client's requirements. If you are capturing more than 20 user stories, your
focus may be too granular. Note that the example user stories above did not
describe things like colors, layout, or font sizes, nor did they prescribe a
particular approach to storing data. (If such things are important to your
client, that's fine, but in most cases a client's desires are not so precise at
this point in the project. That precision will typically come as the result of
seeing and refining the UI designs.)

List your user stories, categorized into the two groups, on your project web
site. If you have a project management board, I recommend also adding the user
stories to it.

### Hints

If the client provides more detail about the features than what's captured in
the user stories, I recommend capturing notes about what the client wants. I
predict that you will be glad you did, especially when it comes time to create
the UI designs.
