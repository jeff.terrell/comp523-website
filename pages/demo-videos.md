---
title: Demo Videos
stem: demo-videos
---

After [Assignment 8: Walking Skeleton](../walking-skeleton/), demo videos will
be due each week at the usual weekly deadline. These are short videos
demonstrating features that have been developed and are now ready for client
review. These videos will be graded on the same 2 point scale used for
assignments.

Most of the time, demos should only be used for features that are substantially
finished. In some cases, you may benefit from including in-progress features in
your demo to solicit client feedback on that feature before it's finished. In
such cases, clearly indicate that the feature is in progress and that you're
only including it to get client feedback of the in-progress work.

If you finished several features in a week, feel free to demo all of them if you
wish. You may do so in one video or several. But demoing a single feature is
sufficient.

### Deployed features

For the best client feedback, the client should be able to exercise the
demonstrated features in the same way as is done in the video. Thus, videos of
deployed features are recommended.

This is more challenging for mobile apps. Depending on your technology stack,
there may be a way to deploy your app without actually releasing it on the app
store. For example:

- For React Native, there's [Snack](https://snack.expo.dev/).
- For iOS, there's [TestFlight](https://testflight.apple.com/).
- For Android, there are [several
  alternatives](https://aloa.co/blog/testflight-ios-beta-testing-android-alternatives-in-2019),
  including the Google Play Console itself.

A client should be able to do the same steps in a production environment as you
show in your video to exercise the feature in the same way.

### Rubric

A video earning the full 2 points is clear and demonstrates the feature as it is
deployed. A video earning 1 point is not clear or demonstrates a feature in a
development environment. Zero points are awarded for missing or late videos.

### Delivery

Link videos from your web site. You can either link to a video file directly
(e.g. in MP4 format) or upload your video to YouTube (or similar) and link to it
there.

### Tips

In most cases, Zoom is an easy way to create such videos. Join a meeting (even
by yourself), share the appropriate window, and record to your computer. Upload
the recording to YouTube or elsewhere and you're done.

For mobile apps, a recording of an emulator window is sufficient so long as the
client can access the feature and the emulator doesn't introduce important
differences from a real phone.

If recording a real phone is needed, I think Zoom might still be a decent
option, although I haven't tried it myself. If all else fails, a video recording
of the app running on a physical phone suffices.

Videos don't have to be long or verbose. In many cases, a 10 second video is
enough to demonstrate a feature. You may even choose to create a (silent)
animated GIF of the feature, which is fine so long as there is sufficient
context around the GIF for the client/mentor/TA to know what's being
demonstrated.
