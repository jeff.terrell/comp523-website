---
title: Fall '21 Lecture Notes - Wed Oct 6 - Interpersonal Effectiveness part 2 and Agility
stem: f21.2021-10-06.agility
---

- slides in [self-contained HTML](../f21/lectures/2021-10-06.agility/slides.html)
- video [on YouTube](https://youtu.be/vI25zgl27fY)
- [transcript of video](../f21/lectures/2021-10-06.agility/transcript.vtt)
