---
title: Fall '20 Lecture Notes - Mon Oct 19 - Testing, Part 1
stem: f20.2020-10-19.testing1
---

- slides ([HTML](../f20/lectures/2020-10-19.testing1/slides.html), [PDF](../f20/lectures/2020-10-19.testing1/slides.pdf))
- video [on YouTube](https://youtu.be/oJd8ewuS1a0)
- [transcript](../f20/lectures/2020-10-19.testing1/transcript.vtt)

