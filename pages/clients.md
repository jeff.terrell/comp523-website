---
title: Info for clients
stem: clients
---

If you can imagine a way for custom software to improve the life or job of you
or others, please consider becoming a client for COMP 523: Software Engineering
Laboratory. Read on for details.

## Contents

- [What is COMP 523?](#what-is-comp-523)
- [What you get](#what-you-get)
- [What you give](#what-you-give)
- [When it happens](#when-it-happens)
- [How to apply](#how-to-apply)
- [What happens next](#what-happens-next)
- [Examples of COMP 523 software projects](#examples-of-comp-523-software-projects)
- [FAQ](#faq)

## What is COMP 523?

In COMP 523, teams of 3–4 senior computer science majors each develop a
semester-long software project for a real client. These clients are people like
you: faculty, staff, other students, or local entrepreneurs or businesses who
have in mind some software that would benefit them or others. I am teaching the
course this fall, and Professor David Stotts teaches it in the spring.

## What you get

You get software developed for free! Typically, clients don't get everything
they hoped for, but they do tend to get functional software that they are
satisfied with.

## What you give

Clients are required to spend a minimal amount of time communicating their
needs. Specifically, as a client, you must:

- describe the project to the instructor and answer any clarifying questions I
  have
- pitch the project to teams (note: not all interested clients are guaranteed
  teams)
- meet with your team several times towards the beginning of the semester to
  develop requirements
- (optional, but recommended) continue meeting with your team each week to
  ensure what's being built is on track with what they need

## When it happens

The fall 2020 semester is already underway. We typically teach the course every
fall and spring semester. If you'd like to be notified when we are next looking
for clients, [email me](mailto:terrell@cs.unc.edu).

Note that early submissions are better than late ones. For one thing, early
submissions allow me time to ask clarifying questions and work with you to help
target your need to the class most effectively. Also, in the event that we have
more project ideas than we need, I will prefer earlier submissions. Anecdotally,
both times I've taught the course, we've had more clients than we need, and I've
had to turn interested folks away&mdash;so for maximum likelihood of getting a
team, don't delay once the application is announced.

## How to apply

_As of Friday, December 10th, 2021, the application is open. Prof. David Stotts
will be teaching COMP 523 next semester. You can [submit a client application
here](https://forms.gle/vMHjZXgzZBTDF7mi9)._

[Email me](mailto:terrell@cs.unc.edu) if you'd like to be notified when the
application opens again.

<!-- Please submit a [client application](https://www.tfaforms.com/4726239). -->

<!-- Feel free to send more than one idea. -->

## What happens next

After I receive your idea, I have several issues to consider. I have to decide
if the software is complicated enough to keep 3&ndash;4 student busy for 3
months. Conversely, I have to evaluate if the idea is too complex for the
timeframe and the size and experience level of the team. Also, I have to
understand to what extent IP, NDA, or HIPAA considerations might apply, which
complicate matters. If there are any issues with your idea, I will typically
try to discuss them with you and see if there's a way to make things fit rather
than rejecting your application outright.

If your project fits well, then I will take the description and put it [on the
projects page](/projects/) for the students to look over, and you will be
invited to pitch your idea in class on Monday, August 24, or Wednesday, August
26. You will get an email asking for your availability and presentation
materials. You will have about 6 minutes to pitch. There may be more pitches
than teams, so pitch well!

Students then decide, individually or as an already-formed team, which projects
they are most interested in. They send me their preferences, which I use to
assign students to projects. I hope to decide and publish assignments on Friday,
August 28.

At that point, if you get a team, you need to meet with them and convey in more
detail what your needs are. This helps orient the team to the project. But the
work doesn't end there. The most successful projects are those in which the
client stays engaged, meeting either weekly or biweekly, to see what's been
done, provide feedback on it, and answer questions from the team. Also, it is
often the case that the team will have new information about unexpected
technical difficulties and revised estimates for how long different features
might take to implement, which naturally leads to a discussion about revised
priorities. For these reasons, I recommend thinking of this as a collaborative
project. You and your team are in a process of discovering a solution to your
problem together.

Towards the end of the semester, the team will deliver the software to you. In
most cases, they will not be available to fix bugs, add features, and maintain
the software after the course ends, so I recommend thinking through what
ownership of the software might be like so you can ask relevant questions while
the team is still available.

For more information and recommendations on being a client, see the [FAQ](#faq)
below.

## Examples of COMP 523 software projects

Here are some examples of software projects completed by past COMP 523 teams.
Please use these ideas for inspiration about what's possible but not an
implicit indication of what isn't allowed.

Most ideas these days tend to be web apps (i.e. available via a web browser
with no software installation needed), mobile apps, or both, but desktop apps,
command-line programs, and other ideas for software are welcome as well.

- A web app to help instructors and staff in the UNC Hospital School (for
  long-term child patients) track student progress.
- A mobile app to help students find and share locations of interest (submitted
  by a pair of UNC student entrepreneurs).
- A web app to help researchers in the Department of Biology visually display
  relationships between proteins.
- A mobile app to share real-time information about the status of UNC parking
  lots (submitted by UNC Parking & Transit).
- A web-based game simulating the experience of an archaeological dig.
- A variety of tools to help professors teach classes (especially large ones)
  more effectively, such as seating chart generators, group managers, and apps
  for registering attendance in class.

Project ideas that have been invited to pitch this semester are listed on [the
projects page](/projects/).

## FAQ

- [How do I apply to be a client?](#client-application)
- [What kinds of projects are a good fit?](#projects-good-fit)
- [What kinds of projects are not a good fit?](#projects-bad-fit)
- [Do I have any restrictions on how I use the software that the students create?](#use-restrictions)
- [I have a confidential data set. Can I get everybody to sign an NDA?](#nda)
- [My app deals with HIPAA-protected data. Is that a problem?](#hipaa)
- [Do projects cost anything?](#costs)
- [How often should I meet with my team?](#meeting-frequency)
- [How much can teams get done in a semester?](#scope)
- [Do you have any advice to help me get working software by the end of the semester?](#advice)
- [What happens to projects after the course is over?](#after-semester)
- [Do you have any advice for the pitch?](#pitch-guidance)
- [What if I have another question, not answered here?](#other-questions)


<a name="client-application" />

#### How do I apply to be a client?

<!--
Submit [an application](https://www.tfaforms.com/4726239) by Wednesday, August
19, 2020.
-->

Submit an application, once the application is announced for a semester. (Email
me to be notified when the application is announced.)

I will evaluate applications, and if there are fixable issues, I will work with
you to resolve them. If your project looks suitable for the course, you will be
invited to pitch your project to the class early in the semester. Students
submit project preferences and are matched with projects, and if you get a
team, you work with them as a client during the semester.


<a name="projects-good-fit" />

#### What kinds of projects are a good fit?

Generally speaking, most COMP 523 projects:

- are _interactive_, responding to the actions of a user;
- have a _graphical user interface_ rather than a text-based one;
- involve _information stored outside of the device_ running the software;
- involve _network requests_ to retrieve and store that information;
- are either a _web app_ (meaning an app you access with your web browser,
  without installing software, like Gmail) or a _mobile app_ (either iOS,
  Android, or cross-platform); and
- are _not vitally important_ to the client, but more like &ldquo;back
  burner&rdquo; or side projects.

However, none of these attributes is required.


<a name="projects-bad-fit" />

#### What kinds of projects are not a good fit?

- Projects that are essential to the survival of a company, or which would
  otherwise place undue pressure on the students.
- Static web sites, which are too simple.
- Projects requiring extensive knowledge in some domain (although in some cases
  you may be the subject matter expert and provide developers the minimal
  knowledge they need).


<a name="use-restrictions" />

#### Do I have any restrictions on how I use the software that the students create?

Technically, work that undergraduate students complete as part of a class is
their own. However, students have a clear understanding that they are building
software for people who need it, and we've never had a greedy student who
refuses to provide the source code and associated materials to the client.

I encourage students to apply a liberal open-source license (e.g. the MIT or BSD
license) to their work, which enables you as a client to take the work, extend
it, and even profit from it.

Projects are rarely innovative enough to develop novel intellectual property. If
an IP agreement is absolutely required for your involvement&mdash;and if we
don't have enough other clients&mdash;we can accommodate you, but note that
some students will not want to work on a project with this characteristic, so
your chances of getting a team are smaller.


<a name="nda" />

#### I have a confidential data set. Can I get everybody to sign an NDA?

This is an unpleasant complication. Some students may be willing to sign an NDA,
but faculty and mentors are not allowed to sign one. Moreover, whether students
are willing to sign an NDA then becomes another factor on a team-to-project
matchmaking process that is already quite complex. Frankly, I am motivated to
work to find clients that do not have these complications.

Instead, I strongly recommend creating a anonymized or sanitized version of the
data set that can be shared freely.


<a name="hipaa" />

#### My app deals with HIPAA- or FERPA-protected data. Is that a problem?

Yes. Students will not have the training necessary to deal with confidential
information, nor can they afford to take the time to learn.

To be a client, I need you to provide sanitized, non-confidential information
that the students can use instead. Once the app is proven to work with sanitized
data in a safe way (an analysis that is outside the scope of this course), then
there are ways of deploying it to a safe and secure environment (again, outside
the scope of this course, but if you already have these concerns, presumably you
have somebody to talk to about them. If not, and you are affiliated with UNC, I
recommend getting in touch with [UNC ITS](https://its.unc.edu/) for both
deployment and security auditing questions).


<a name="costs" />

#### Do projects cost anything?

There is no fee for participating in the class, so this is an unusual
opportunity to get software developed &ldquo;for free&rdquo;. However, two
caveats apply:

- You will need to spend some time on the project.
- Students cannot be expected to pay for incidental expenses, so you should be
  prepared to pay for such things as necessary. This would include minor hosting
  costs of maybe $10-20 per month in some cases (although I encourage students
  to use free servers from [Heroku](https://heroku.com) that won't cost
  anything). Also, in order to publish a mobile app on an app store, you need a
  developer account, which, last I checked, was $25/lifetime for Android and
  $99/year for Apple.


<a name="scope" />

#### How much can teams get done in a semester?

It can vary quite a bit based on the students' skills at programming and
collaborating, but in general, most teams can finish a basic version of a web or
mobile app by the end of the semester. Typically, teams struggle to actually
publish an app in an app store, partially because much of that work needs to be
done by clients, and by the time everybody starts thinking about that, it's too
late. Note that the app will almost certainly not have all the functionality you
hoped for. However, you are welcome to apply to be a client again the following
semester to add some more features to your app.


<a name="meeting-frequency" />

#### How often should I meet with my team?

I recommend meeting biweekly for an hour, with an emphasis on reviewing the
work so far&mdash;*not* as a manager trying to drive progress, but as a humble
recipient of a free service, merely to provide feedback on whether what you see
effectively captures what you want. Meeting more often is usually a challenge
for the team, not just in terms of more time spent in meetings (and therefore
not developing the software), but also because it can be stressful to have to
prepare a demo every week. Less often can also be a challenge because:

- you have fewer total opportunities to provide feedback and home in on an app
  that meets your needs;
- the work you're giving feedback about is staler, so not as fresh on the
  students' minds, which means they might not remember some of the important
  reasons why they did things the way they did; and
- students may have built more things on top of something you want to change,
  which can waste time and energy and feel frustrating to you and them.

I also recommend telling your team that they should ask you clarifying questions
about what you want as things come up, and do your best to reply to any
questions promptly. This kind of outlet can greatly help reduce wasted effort
and keep the team on track with their progress.


<a name="advice" />

#### Do you have any advice to help me get working software by the end of the semester?

Yep! First, read the previous question and answer.

Second, try to identify the smallest set of features that could constitute an
app that is useful. Be ruthless about moving things to the &ldquo;do
later&rdquo; category. Then clearly communicate to your team about what the
essentials are. This helps you and the team focus on getting something working.
Once those features are done, you can start to add more, starting with the most
valuable next feature. Having the essentials done is an important milestone
that tends to give an extra bit of excitement to the project.

Third, don't pressure the team to get more done or to get things done faster.
That's not your job, and you can trust the mentors and the course structure to
drive progress. If you push too much, you and the team will probably get
stressed and frustrated, and we might not invite you back after the semester is
over.

Fourth, understand that you have some amount of control over your time
preference: that is, to what extent you want lower-quality things sooner or are
willing to wait until later for higher-quality things. Sometimes trying to cram
in that last feature or three before the deadline means that the code is in a
rather fragile state, perhaps with more bugs or insufficient tests. Building
the next feature on top of that might require spending significantly more time
cleaning up the mess than it would have taken to do it well the first time. For
this reason, it can be wise to encourage teams to spend the last couple of
weeks just cleaning house and organizing the code so that it will be easier for
the next developers to understand and extend it. You get fewer features now,
but it's easier to add more features later.

Finally, appreciate your team. Thank them often for all their efforts. Maybe
bring donuts to your meetings. Besides it just being considerate, it also helps
establish a friendlier and more collaborative atmosphere. And you never know,
maybe one of the students might be willing to stick around after the semester is
over and fix the odd bug or two.


<a name="after-semester" />

#### What happens to projects after the course is over?

Students are done with the course. Although they often fondly remember the
experience for the rest of their lives&mdash;and indeed, many alumni report that
it was their favorite course&mdash;they are never expected to do any work on the
project after the semester is over.

After they're gone, the project is in your hands. They should have delivered the
source code to you, as well as [documentation](/client-overview/) written so
that you can understand how the app works at a high level. You might want to
find another developer to continue the work. You might want to apply to be a
client next semester. You might have accomplished your goals and be ready to use
and maybe publish the app. It's up to you.


<a name="pitch-guidance" />

#### Do you have any advice for the pitch?

Yes. I recommend motivating the problem in human terms first by talking about
who is experiencing pain and how the proposed software will help. Then discuss
what form you're envisioning the software would take. You might benefit from
reviewing the questions in the initial client application, as well as your
answers, which were emailed to you when you applied.

Remember that you have a short time, maybe 6 minutes, to connect to
significance and get students excited about your project, so make your words
count. Personally, I tend to have the best results when I rehearse a talk like
this as many as 10 or 15 times until it's polished and smooth (seriously). You
can leave time for questions if you want, but know that, in most pitches I've
observed, there weren't any.

Here are some things that students might be asking themselves about your
project:

- Do I (or can I) care about this problem or the people who would benefit?
- Does this client seem like somebody I would enjoy working with, and who
  wouldn't get frustrated if things aren't going as well as they hoped?
- Does this client seem flexible and open to input about what works well at the
  technical level (and what doesn't)?
- Will I be able to use my favorite programming language or tool?
- Will I get to learn about a new programming language or tool, especially one
  that might look good on my r&eacute;sum&eacute;?
- Does the project seem overwhelming in size and scope? Does the client
  understand that they probably won't get everything they're hoping for done
  this semester?
- Is there anything intimidating about this project? Maybe a technology or
  piece of hardware that I know nothing about and that sounds hard to learn?


<a name="other-questions" />

#### What if I have another question, not answered here?

[Email me](mailto:terrell@cs.unc.edu). For more involved discussions, I also
try to make myself available (to students, clients, entrepreneurs, etc.) during
office hours. Check [my web site](http://terrell.web.unc.edu/) for current
details.
