---
title: Assignment 14: Apples Reflection 2
stem: apples-reflection-2
---

### Due

Monday, November 22<sup>th</sup> at 8am.

### Context

This course counts as an [APPLES Service
Learning](https://ccps.unc.edu/apples/) course. As such, we need occasionally
to reflect on its service aspect.

### Requirements

Post a brief (at most one page) essay response to the following prompt on your
project web site:

> Earlier in the semester, you were asked to reflect on the organization that
> is your client and what they are trying to accomplish. It is now the end of
> the semester and you have been interacting with them on a regular basis. Has
> your impression of their purpose or clientele changed? How? Are you more or
> less excited about their mission? Explain how your product is helping their
> mission. Describe the features that have been most important to your client
> and explain how they address their intended mission.
