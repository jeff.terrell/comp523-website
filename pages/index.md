---
title: Welcome
stem: index
---

Welcome to COMP 523: Software Engineering at UNC. Typically, we offer COMP 523
every fall and spring semester. We hope to offer it in the fall 2022 semester,
but plans are not yet determined.

In this course, teams of 3–4 upper-level computer science majors build software
for a real client with a real software need. These are not contrived class
assignments, but real software projects, and every project is different.

**If you are a student in COMP 523**, most of the content on the site is
written for you.

**If you have a problem that software could solve**, you could be a client in
the class, and a student team could work on your idea during a fall or spring
semester. To learn more, read [the clients page](../clients/).

**If you are a software engineer who wants to give back**, you could be a
mentor for a student team. Mentors meet with their team each week to help them
work through the challenges of developing a software project and collaborating
with their client and each other. To learn more, read [the mentors
page](../mentors/).
