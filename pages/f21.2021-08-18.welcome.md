---
title: Fall '21 Lecture Notes - Wed Aug 18 - Course Overview
stem: f21.2021-08-18.welcome
---

- video [on YouTube](https://youtu.be/VoRFWPUEbeQ)
- [transcript of video](../f21/lectures/2021-08-18.welcome/transcript.vtt)

---

# Course website

[https://comp523.cs.unc.edu](https://comp523.cs.unc.edu) (about the most
obvious URL possible, no?)

- See [the official syllabus](https://comp523.cs.unc.edu/syllabus/) on the
  class website: [https://comp523.cs.unc.edu](https://comp523.cs.unc.edu)
- The following notes do not replace the syllabus

---

# Introductions - me

- My job title: _Professor of the Practice_
- No research area, but I run the [UNC App Lab](https://applab.unc.edu)
- I've been building apps for 15+ years now
- I live on 4+ acres north of Chapel Hill, with wife, 3 kids ages 8-4, 1 dog, 1
  cat, (currently) 5 goats, and a variable number of chickens
- I enjoy reading, listening to music or podcasts, watching movies/TV, and running
- Favorite programming language: Clojure (pragmatic functional programming)
- Editor of choice: Spacemacs (fully-featured Vim-style editing in Emacs)

---

# Introductions - you

Please visit [PollEv.com/jeffterrell](https://pollev.com/jeffterrell) for a
brief poll.

(Polls count for class participation credit so should not be considered
optional.)

---

# Classroom policies

- In-person + Zoom simulcast; either way is fine
- Recordings will be posted, but I expect synchronous attendance
- If you're sick, stay home (and get tested)
- If I'm sick, I'll stay home (and communicate a plan for holding class)
- Polls and in-class activities for both in-person and Zoom attenders
- These are required for class participation credit
- Please don't distract others from learning with your voice, your phone, your
  screens, the Zoom chat, etc.
- To ask questions on Zoom: raise your hand or ask in the chat window
- I have refrained from posting the class Zoom link publicly; if you need it,
  email me

---

# Why take COMP 523?

- Experience
    - Get practical experience _building software with others_
    - Super relevant for those intending a software development career
    - Put your project on your résumé to land a good job
- Meaning
    - Your client needs the software you'll be building; this isn't
      hypothetical
- Fun
    - Department chair Kevin Jeffay informally reports that many alumni he
      meets say that COMP 523 was their favorite course in UNC Computer
      Science
- Over two thirds of responding students last fall strongly agreed with the
  statement, "More CS students should take COMP 523."

---

# About COMP 523

This course is unusual:

- It prioritizes _application_ over _concepts_
- It is highly _collaborative_
- It is more directly relevant to professional employment

Target audience: upper-level (junior and senior) CS majors

You will be building _real software for real people_ in this course!

**Key idea:** you will team up in groups of (probably) 3 or (maybe) 4 for the
duration of the semester to work on your project.

**Poll:** do you have teammates in mind already?
[https://pollev.com/jeffterrell](https://pollev.com/jeffterrell)

---

# Learning objectives

> At the end of the course, each student will have experienced all aspects of a software development project, including:
>
> - working with a client to define goals and priorities
> - designing a system
> - scheduling and planning a multi-person project
> - effective communications
> - running meetings
> - writing technical documentation
> - preparing web content
> - writing and testing code
> - deploying the system
> - public presentations

---

# Collaboration

There are 3 types of people you will collaborate with this semester:

1. Your teammates
2. Your client: the one who wants the software you'll build, perhaps a UNC
   professor or researcher or a local entrepreneur
3. Your mentor: somebody to help you along the way, either TA Vraj or a
   professional software engineer

### More on mentors

- Note that mentors wear a management hat, reporting to me on completion of
  assignments (but they don't assign grades).
- My hope is that the third-party aspect of mentors will help counter some of
  the shame that can accompany discussion of problems and failures. (We all
  fail! It's OK!)
- Problems with your mentor? Talk to me.

### Team/client/mentor assignments

- Next week during class, you'll watch all the clients pitch their projects
- You'll submit your team/project preferences by next Friday, 8am
- I'll assign teams and clients based on your preferences, to be revealed
  next Friday during class
- Also next Friday: assigning mentors based (purely) on team availability

### Meetings

- 1 required meeting each week with mentor
- I strongly recommend also scheduling a time to meet synchronously with your
  team
- Plus, at least one team member will need to meet at least occasionally with
  your client (I recommend bi-weekly, but it depends on their preference)
- Finding times for meetings can be challenging!
- But you have an ace in the hole: Friday class time
- All of you currently have that time free
- We won't be meeting on Fridays after next week
- **Please keep that time available for meetings**
- Plan on meeting with your team, your mentor, or both during that time

### Talks

- This is a communication intensive course!
- Besides the weekly meetings, your team will deliver 3 talks during the
  semester
- First, a 5-min midterm presentation: describe your project and show progress
  so far
- Second, a 20-min tech talk: teach some tool or technology to the class
- Third, a 9-min final presentation, including lessons learned
- Each team member must participate in each talk

### Honor code

- Collaboration on your project and talks is _expected_ and even _required_ in
  COMP 523
- Collaboration of any sort on the quizzes is prohibited

---

# Grading policies

### Grading weights

- **10%** for attendance and participation in class meetings (5%) and mentor meetings (5%)
- **10%** for quizzes
- **15%** for the tech talk
- **65%** for the project

### Grading cutoffs

- No rounding: 89.5% is a B+, not an A-
- Cutoff for A is 95.0%, not 93.0%

### Attendance/participation grades

- 10% of course grade
- Half for class meetings
    - Participation in polls and in-class activities
    - Credit whether you get the answer right or wrong
    - Each class meeting weighted equally
    - Beware polls at beginning and end of class
- Half for mentor meetings
    - Did you show up? (half credit)
    - Were you on time? (half credit)
    - Each mentor meeting weighted equally
- Reasonable excuses will be approved **in advance**
- Job interviews and career fairs are not valid excuses (but I recommend
  skipping class for such things anyway :-) )

### Quiz grades

- 10% of course grade
- My belief is that a complete lack of accountability for lecture content does
  you a disservice
- Quizzes will be worth 10% of your total grade
- Plan for now: a 10-minute quiz during class, every other week
- My aim: if (and only if) you paid attention during lectures, you should do
  OK without studying
- Quizzes will also include content from the required textbook
    - This is not difficult reading but should be a big help to your project
      work
    - See the calendar for when you should read chapters by
    - _Debugging: The 9 Indispensable Rules for Finding Even the Most Elusive
      Software and Hardware Problems_ by David J. Agans

<img
  src="../f21/lectures/2021-08-18.welcome/debugging-agans.jpg"
  alt="cover of debugging book by David J. Agans"
/>

### Project grades

- 65% of course grade
- Half for assignment grade
    - Were assignments completed on time (even if not perfect)?
    - More on assignments below
- Half for qualitative app grade
    - An app getting an A grade:
        - is well designed and attractive;
        - has an intuitive user interface;
        - is deployed;
        - includes at least the most important features;
        - has good test coverage (probably over 70%, although that can depend
          on the project), and
        - is free of obvious bugs and errors.
    - App getting a B might miss 2 of those criteria; A \"C\" app has
      significant problems
    - Note: app grading is intended to be somewhat lax, as a concession for the
      workload
- 1 project grade is assigned per team; however:

### Individual grade multiplier

- Your individual project grade has an additional factor
- If you really slacked off and let your teammates down, your project grade
  will be multiplied by a factor as low as 0.7
- If you really went above and beyond (_while still collaborating well with
  teammates_), your project grade might be multiplied by 1.1 (rare)
- In order to get a 1, you must:
    - meet all role responsibilities
    - attend and participate in all meetings
    - commit to a reasonable amount of work
    - deliver on your commitments
    - help team members when appropriate
    - not preempt work from others nor denigrate their contribution
- In the past, most students had a multiplier of 1.0

### Assignments

- Most weeks, a team assignment will be due
- Assignments are due (soft deadline) on Sunday night
- But won't actually be checked until (hard deadline) your mentor meeting that
  week
- Remember: almost a third of your course grade (32.5%) is from _on-time_
  delivery of these assignments
- Note: better to deliver something mediocre on-time than something great late
- The structure of assignments is intended to be helpful and counter our shared
  tendency to procrastinate :-)
- You are encouraged to start on substantive project work early, however
- No assignments due this week; I'll remind you later

---

# Miscellaneous

### Getting help

- No Piazza instance this time (it wasn't useful last fall)
- Use your mentors! They're here to help.
- For course or assignment related questions, first ask TA Vraj
  (patelvap@live.unc.edu). You can also ask me (terrell@cs.unc.edu) if needed.

### Dealing with stress

- This can be a stressful course:
    - collaboration: choices of others can affect your grade
    - conflict: preferences and opinions can clash, esp. among your team
    - impact: you can disappoint not only yourself but also your team, your
      mentor, and your client
- If your stress or anxiety is interfering with your life or sleep in an
  unusual way, I encourage you to get help
- No shame: I've gotten lots of help from therapists in the past, and still see
  one regularly
- To get help, see the [UNC Counseling And Psychological Services (CAPS)
  website](https://caps.unc.edu/).

### Accessibility Resources and Services (ARS)

> The University of North Carolina at Chapel Hill facilitates the
> implementation of reasonable accommodations, including resources and
> services, for students with disabilities, chronic medical conditions, a
> temporary disability or pregnancy complications resulting in barriers to
> fully accessing University courses, programs and activities.

> Accommodations are determined through the Office of Accessibility Resources
> and Service (ARS) for individuals with documented qualifying disabilities in
> accordance with applicable state and federal laws. See the ARS website for
> contact information or email ars@unc.edu.

### Title IX Resources

> Any student who is impacted by discrimination, harassment, interpersonal
> (relationship) violence, sexual violence, sexual exploitation, or stalking is
> encouraged to seek resources on campus or in the community. Please contact
> the Director of Title IX Compliance (Adrienne Allison –
> Adrienne.allison@unc.edu), Report and Response Coordinators in the Equal
> Opportunity and Compliance Office (reportandresponse@unc.edu), Counseling and
> Psychological Services (confidential), or the Gender Violence Services
> Coordinators (gvsc@unc.edu; confidential) to discuss your specific needs.
> Additional resources are available at safe.unc.edu.

### Dropping the course

Please don't drop after teams are assigned! This will be a difficult situation
for your assigned teammates.
