---
title: Assignment 15: Personal Report
stem: personal-report
---

### Due

Wednesday, December 1<sup>st</sup> by 11:59pm.

### Context

I need to know how things really went within your team. Were there any
slackers? Were there any significant problems? This information helps me assign
individual grades. Also, I need visibility about how mentors are doing so I can
decide who to invite back next time.

### Requirements

Email me ([terrell@cs.unc.edu](mailto:terrell@cs.unc.edu)) a short personal
report with the following information:

- **project** - Your impression of how the project went. I have seen the team
  view already (your final team presentation in class). Now I want your
  personal view. What worked well? What didn't work well? This is your chance
  to tell me anything you want me to know about the project.
- **team** - Give me an honest assessment of how the team functioned. Did
  everyone pitch in and contribute? Do this by &ldquo;paying&rdquo; everyone,
  yourself included. You have $100 to spend. Please allocate the money among
  the team members. Justify your decisions and give me your review of how well
  each team member contributed. Don't worry about breaking this down to hours
  and minutes; just give me a rough assessment of whether each person did a
  fair share of the work. *This is not an invitation to be rude. Be fair and
  honest, but be polite to your colleagues.*
- **mentor** - Was your mentor helpful? Were there aspects of your mentoring
  relationship that worked well, or that didn't work well? Should I invite this
  person back to be a mentor in future semesters?
- **client** - Was your client easy to work with? Were they strict, demanding,
  or pushing you hard to get as much out of you as they could? Should I invite
  this person back to be a client in future semesters?
