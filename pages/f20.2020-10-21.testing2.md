---
title: Fall '20 Lecture Notes - Mon Oct 21 - Testing, Part 2
stem: f20.2020-10-21.testing2
---

- slides ([HTML](../f20/lectures/2020-10-21.testing2/slides.html), [PDF](../f20/lectures/2020-10-21.testing2/slides.pdf))
- code [on GitLab](https://gitlab.com/jeff.terrell/testing-tutorial/)
- video [on YouTube](https://youtu.be/AHssbYo7Thw)
- [transcript](../f20/lectures/2020-10-21.testing2/transcript.vtt)

