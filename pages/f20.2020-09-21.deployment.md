---
title: Fall '20 Lecture Notes - Mon Sept 21 - Deployment
stem: f20.2020-09-21.deployment
---

- slides ([HTML](../f20/lectures/2020-09-21.deployment/slides.html), [PDF](../f20/lectures/2020-09-21.deployment/slides.pdf))
- video [on YouTube](https://youtu.be/ubZC6bzuPCY)
- [transcript](../f20/lectures/2020-09-21.deployment/transcript.vtt)
