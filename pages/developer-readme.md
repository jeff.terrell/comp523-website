---
title: Assignment 12: Developer README
stem: developer-readme
---

### Due

Monday, November 15<sup>th</sup> at 8am.

### Context

Many COMP 523 projects live on in some capacity after the semester ends.
Sometimes there's a follow-up project next semester. Other times, the client
finds another developer to work on the project. In anticipation of a new
developer starting work, it's helpful to write a `README` that tells the
developer what they need to know.

Furthermore, a `README` is a common part of real software projects, and it's
useful to know how to write and maintain one.

### Requirements

Add a `README.md` file to the root directory of your repository (or, if one
already exists, update it). Note that this file will be in [Markdown
format](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet). The
`README.md` should have the following outline:

0. &lt;Project title&gt;

    - A tweet-sized (280 characters max) summary of why the project exists.

1. Getting started

    - Prerequisites: what other software needs to be installed before developing
      or running this software (e.g. Node.js)? Give specific commands to install
      the software, noting any assumptions about package management tools or
      operating system.
    - Installing: how can a user get a working development environment? Give
      step-by-step instructions.
    - Running locally: once everything is installed, how can a user start a local
      server?
    - Warranty: when (in terms of date or commit) were these instructions last
      tested and verified to work, by whom, and on what operating system?

2. Testing

    - How can the user run the test suite? Give specific commands.
    - Any other test-related commands to know about, e.g. a different command for
      unit tests vs. integration tests?

3. Deployment

    - Where does the production system live? How would a new developer get access
      to it?
    - Are there any staging or pre-production environments that developers can
      use?
    - What are the various pieces that the fully deployed software uses? For
      example, with Heroku, what addons does the app use?
    - Is continuous integration or continuous deployment enabled? If so, where
      does it live?

4. Technologies used

    - List which technologies were chosen.
    - Mention where in the repository the ADRs live. (Add them to the repository
      if they're not already there.)

5. Contributing

    - Does a new developer need to get access to any systems (e.g. the GitHub
      repository, Trello) before he or she is able to contribute?
    - Are there any style, testing, or process conventions that a new developer
      should know about?
    - Include a link to your project web site for more background information.

6. Authors

    - Who have the major authors been so far? This is your chance to acknowledge
      your own efforts.

7. License

    - What license applies to the source code? I recommend a liberal
      open-source license like the MIT license or the BSD license, so that your
      client can have the legal right to use and extend your work later in
      whatever way they would like. (However, teams with an IP agreement with
      their client should be careful not to pick something without getting
      their client's approval first.)

8. Acknowledgements

    - Is there anybody else you'd like to thank, or any resources that have been
      especially helpful?

Add a link to your repository to your project web site.

### Hints

You may want to use a Markdown preview tool to ensure before committing that
your Markdown renders the way you expect. There are many such tools available.
