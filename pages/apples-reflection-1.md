---
title: Assignment 5: Apples Reflection 1
stem: apples-reflection-1
---

### Due

Sunday, September 27<sup>th</sup> at 8am

### Context

This course counts as an [APPLES Service
Learning](https://ccps.unc.edu/apples/) course. As such, we need occasionally
to reflect on its service aspect.

### Requirements

Post a brief (at most one page) essay response to the following prompt on your
project web site:

> You are developing software for an organization that needed some help. Some
> of these organizations are non-profits, some are university entities, some
> are creating a business that meets an unfilled need. You have now spent over
> a month working with this client and should understand their goals and how
> your project fits into these goals. All of them have the intention of making
> a difference. As part of the service-learning component of this course, you
> are to identify what the goals and values of your project are. Clearly, not
> all of the projects are out to solve world hunger, but they are all meeting
> some population's needs. Identify the need that you are meeting. If you are
> supporting researchers, you can explain why the research that they are doing
> is important and whom it affects. Explain the importance of this need and how
> the work that you are doing will impact it.
