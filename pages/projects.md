---
title: Projects
stem: projects
---

Here are the projects for the fall 2021 semester.
You can also see the projects for
[fall 2019](../2019-projects/) and
[fall 2020](../2020-projects/).

### Contents

- [Projects](#projects)
- [Mentor assignments](#mentor-assignments)
- [Pitch recordings](#pitch-recordings)
- [Intellectual property](#intellectual-property)
- [Project details](#project-details)

### Projects

After a team finishes [Assignment 2](../web-site/), their web site will be
linked here.

| team | lead client     | students                        | web site                                                                         |
|------|-----------------|---------------------------------|----------------------------------------------------------------------------------|
| A    | Brumfield       | Bahali, Chow, Khadri, Narvekar  | [web site](https://honors-carolina-resume-app.web.app/)                          |
| B    | Clark           | Ya. Hu, Wasyluk, Y. Zhang       | [web site](https://uncch.notion.site/SweepStat-d0259ba917ad4c7a8bc5e508f663ac7d) |
| C    | Maddox          | Atay, Khan, Lash                | [web site](https://tarheels.live/strengthn/)                                     |
| D    | Toben           | Pendzick, Peng, Prendergast     | [web site](https://tarheels.live/littermapproject/)                              |
| E    | Barron          | Dreibelbis, Hinson, Sungaila    | [web site](https://tarheels.live/wavecomp523fa21/)                               |
| F    | Simon           | Dod, K. Li, Manila              | [web site](https://tarheels.live/comp523collectsmart/)                           |
| G    | Stone           | Decker, Goehring, Wortham       | [web site](https://jackowfish.github.io/big-words-site/)                         |
| H    | Radsky          | Huang, P. Liu, M. Zhang         | [web site](https://comp523-fall21-h.herokuapp.com/)                              |
| I    | Halpin          | Lin, C. Zhang, Zhou             | [web site](https://comp523teami.github.io/TEAMI/)                                |
| J    | McDaniel        | Koonce, Maatallah, Shields      | [web site](https://tarheels.live/comp523f21j/)                                   |
| K    | Hu              | Bennett, Groh, Zeplin           | [web site](https://tarheels.live/xaitkprojectportfolio/)                         |
| L    | Shah            | Bucher, Post, Qualkenbush       | [web site](https://wbucher3.github.io/comp-523-info-website/)                    |
| M    | Barber          | L. Le, Lei, B. Li, Lu           | [web site](https://tarheels.live/holind/)                                        |
| O    | Gordon-Larsen   | Bautista, Chun, Lewis           | [web site](https://tarheels.live/comp523f21o/)                                   |
| P    | Yu              | Dockendorf, Kim, Rahmouni       | [web site](https://ousrahm.github.io/Motivation-Website/)                        |
| Q    | Hurlbert        | Chou, Him, K. Le                | [web site](https://tarheels.live/comp523project/)                                |
| R    | Walls           | Yi. Hu, Wen, Yang               | [web site](https://tarheels.live/teamrfanzplay)                                  |
| S    | Gsell           | Chen, Haveliwala, Puri          | [web site](https://tarheels.live/carolinacafereserve/)                           |
| T    | Harris          | Hawkins, Ho, Jagadeesan         | [web site](https://tannerhawkins.github.io/Comp523-Website/home/)                |
| U    | Steelman        | Almvide Lundberg, Lempp, Parker | [web site](https://samlempp.github.io/ZIP-Code-Lookup/)                          |
| V    | Joseph-Nicholas | Habib, Lynch, N. Nguyen, Yalcin | [web site](https://tarheels.live/csulatraining/)                                 |
| W    | Maitland        | Batchelor, Maszer, Venerella    | [web site](https://tarheels.live/whereistheplay/)                                |
| Z    | King            | X. Li, Y. Liu, Zhao             | [web site](https://teamz-comp523.github.io/vcp/)                                 |
| α    | Trembath        | Hayes, T. Nguyen, Rubido Rafull | [web site](https://cdrubido9307.github.io/web-comp523/index.html)                |
| β    | Weaver          | Revis, Serrato Rojas, Willen    | [web site](https://smartwandsite.herokuapp.com/)                                 |
| γ    | Gopalakrishna   | Drewery, Hartmark, Nukala       | [web site](https://tarheels.live/teamgammacomp523/)                              |


### Mentor assignments

Students, please let me know when a meeting time has been established and I will
post it here.

| team | mentor      | meeting time |
|------|-------------|--------------|
| A    | Ginsberg    | Tue 3pm      |
| B    | Camenisch   | Sat 1pm      |
| C    | Cowart      | Wed 5:30pm   |
| D    | Byrne       | Mon 5pm      |
| E    | Anderson    | Fri 10am     |
| F    | Fletcher    | Fri 9am      |
| G    | Farmer      | Wed 6:30pm   |
| H    | Pollack     | Fri 1:30pm   |
| I    | Gramann     | Mon 3pm      |
| J    | Dinger      | Mon 5pm      |
| K    | Woods       | Fri 1:30pm   |
| L    | Lake        | Thu 3:30pm   |
| M    | Doshi       | Fri 1:30pm   |
| O    | Knoble      | Fri 5pm      |
| P    | Killen      | Fri 1:30pm   |
| Q    | Chadwell    | Mon 3:30pm   |
| R    | Brown       | Fri 2pm      |
| S    | Wright      | Fri 2pm      |
| T    | Nguyen      | Wed 10am     |
| U    | Nielsen     | Fri 2pm      |
| V    | Yackenovich | Fri 1pm      |
| W    | Rasch       | Fri 2pm      |
| Z    | Patel       | Mon 3:45pm   |
| α    | Byzek       | Fri 1:30pm   |
| β    | Cremins     | Sat 12pm     |
| γ    | Patel       | Tue 2pm      |


### Pitch recordings

- [Mon Aug 23](https://youtu.be/4Lb1rRzjLIw)
- [Wed Aug 25](https://youtu.be/LibqxfKlUiE)


### Intellectual property

Clients marked with <sup>**IP**</sup> in the list below have indicated that they
are sensitive to issues of intellectual property and/or confidentiality. You can
read their response about particular concerns in their application. Such
projects will require students to review and sign a legal agreement to
participate in the project. Students are not required to participate in
IP-sensitive projects, and their grade is in no way affected by their choice of
whether to engage in such a project. The agreement has been written by the UNC
Office of Technology Commercialization (OTC) with this particular situation (and
students' needs) in mind. For more information, see the [participation
guidelines](../f20/clients/ip-guidelines.docx). You can also view the
[participation agreement](../f20/clients/ip-agreement.docx) itself. (It's
short.) For questions, please contact [Peter
Liao](https://otc.unc.edu/people/peter-liao/) &lt;liaopb@unc.edu&gt;,
Commercialization Manager at UNC OTC, who will also attend class on Fri Aug 20
to discuss these issues.


### Project details

Project titles (provided by the client) and lead client names are given in
pitch order below, with links to the client-submitted application and any
additional materials provided by the client. Project summaries (provided by
clients) are further below.

The applications are responses to [this form](https://www.tfaforms.com/4726239),
which you can see for more details on what questions are being asked, etc. You
can page through the application, but please don't submit an application.

Projects marked <sup>**iOS**</sup> include iOS development work, so at least one
team member must have access to MacOS. Projects marked <sup>**iOS-only**</sup>
are exclusively iOS development work, so **all** team members must have access
to MacOS.

|   | client                         | title                                                                                          | details                  | pitch                                                                        | application                               | additional materials                                     |
|---|--------------------------------|------------------------------------------------------------------------------------------------|--------------------------|------------------------------------------------------------------------------|-------------------------------------------|----------------------------------------------------------|
| A | Brumfield                      | Honors Carolina Database For Resumes & Beyond                                                  | [link](#brumfield)       | [pdf](../f21/clients/brumfield-slides.pdf)                                   | [pdf](../f21/clients/brumfield.pdf)       |                                                          |
| B | Clark, Verber                  | <sup>**iOS**</sup> Sweepstat Mobile App                                                        | [link](#clark)           | [pptx](../f21/clients/clark.pptx)                                            | [pdf](../f21/clients/clark.pdf)           | -                                                        |
| C | Maddox                         | Strength^n, for meaningful connections                                                         | [link](#maddox)          | [pdf](../f21/clients/maddox-slides.pdf)                                      | [pdf](../f21/clients/maddox.pdf)          | [.mov file](../f21/clients/maddox.mov)                   |
| D | Toben, Sagdejev                | Litter Map GIS Rendering                                                                       | [link](#toben)           | [pptx](../f21/clients/toben.pptx)                                            | [pdf](../f21/clients/toben.pdf)           | -                                                        |
| E | Barron, McHargue, Duperier     | <sup>**IP**</sup> <sup>**iOS-only**</sup> WAVE                                                 | [link](#barron)          | [pptx](../f21/clients/barron.pptx)                                           | [pdf](../f21/clients/barron.pdf)          | [keynote file](../f21/clients/barron.key)                |
| F | Simon                          | Collect SMART                                                                                  | [link](#simon)           | [pptx](../f21/clients/simon.pptx)                                            | [pdf](../f21/clients/simon.pdf)           | [poster (pdf)](../f21/clients/simon-poster.pdf)          |
| G | Stone                          | <sup>**IP**</sup> BigWords                                                                     | [link](#stone)           | [pptx](../f21/clients/stone.pptx)                                            | [pdf](../f21/clients/stone.pdf)           | [.mov file](../f21/clients/stone.mov)                    |
| H | Radsky, Moon                   | Easy Access Counselor Portal                                                                   | [link](#radsky)          | [pptx](../f21/clients/radsky.pptx)                                           | [pdf](../f21/clients/radsky.pdf)          | [EasyAccess intro video](https://youtu.be/_Az2E7qMu10)   |
| I | Halpin                         | Online Peer Learning for Intro Stats                                                           | [link](#halpin)          | [pdf](../f21/clients/halpin-slides.pdf)                                      | [pdf](../f21/clients/halpin.pdf)          | -                                                        |
| J | McDaniel, Watkins              | <sup>**IP**</sup> Health Equity Project (saving lives!!)                                       | [link](#mcdaniel)        | [pptx](../f21/clients/mcdaniel.pptx)                                         | [pdf](../f21/clients/mcdaniel.pdf)        | [wireframe (png)](../f21/clients/mcdaniel-wireframe.png) |
| K | Hu, Tunison                    | XAITK: The Explainable AI Toolkit                                                              | [link](#hu)              | [YouTube](https://youtu.be/0hzoQXAYchA)                                      | [pdf](../f21/clients/hu.pdf)              | [slides (pptx)](../f21/clients/hu.pptx)                  |
| L | Shah, Eshet, Konz              | <sup>**IP**</sup> Personalized Reminiscence Therapy                                            | [link](#shah)            | [YouTube](https://youtu.be/IHJd_1mc_vs)                                      | [pdf](../f21/clients/shah.pdf)            | -                                                        |
| M | Barber                         | HOLIND: Mobile app to facilitate at home treatment for disabilities                            | [link](#barber)          | [video](../f21/clients/barber.mp4)                                           | [pdf](../f21/clients/barber.pdf)          | [slides (pptx)](../f21/clients/barber.pptx)              |
| O | Gordon-Larsen, Kelley          | Software for Research Impact                                                                   | [link](#gordon-larsen)   | [YouTube](https://youtu.be/3MvakFrCIeU)                                      | [pdf](../f21/clients/gordon-larsen.pdf)   | -                                                        |
| P | Yu, Yang                       | <sup>**IP**</sup> Motivation                                                                   | [link](#yu)              | [pptx](../f21/clients/yu.pptx)                                               | [pdf](../f21/clients/yu.pdf)              | [report (pdf)](../f21/clients/yu-report.pdf)             |
| Q | Hurlbert                       | Avian Diet Database                                                                            | [link](#hurlbert)        | [pptx](../f21/clients/hurlbert.pptx)                                         | [pdf](../f21/clients/hurlbert.pdf)        | -                                                        |
| R | Walls                          | FANzPlay...The Game within a Game!                                                             | [link](#walls)           | [pptx](../f21/clients/walls.pptx)                                            | [pdf](../f21/clients/walls.pdf)           | -                                                        |
| S | Gsell                          | Cafe Reservation System                                                                        | [link](#gsell)           | [pptx](../f21/clients/gsell.pptx)                                            | [pdf](../f21/clients/gsell.pdf)           | -                                                        |
| T | Harris                         | Augmented Interactive Education Platform                                                       | [link](#harris)          | [YouTube](https://youtu.be/0fkpG8Qu4kI)                                      | [pdf](../f21/clients/harris.pdf)          | -                                                        |
| U | Steelman                       | ZIP Code Lookup Tool                                                                           | [link](#steelman)        | -                                                                            | [pdf](../f21/clients/steelman.pdf)        | -                                                        |
| V | Joseph-Nicholas                | Online Training for CS ULAs                                                                    | [link](#joseph-nicholas) | [pptx](../f21/clients/joseph-nicholas.pptx)                                  | [pdf](../f21/clients/joseph-nicholas.pdf) | -                                                        |
| W | Maitland                       | Where's the Play?                                                                              | [link](#maitland)        | [pptx](../f21/clients/maitland.pptx)                                         | [pdf](../f21/clients/maitland.pdf)        | -                                                        |
| Z | King                           | VCP Volumetric Capture Processer App                                                           | [link](#king)            | -                                                                            | [pdf](../f21/clients/king.pdf)            | -                                                        |
| α | Trembath, Hornsby, Stadnik     | No More Indecipherable Doctor Handwriting                                                      | [link](#trembath)        | [YouTube](https://youtu.be/joDiExmS7wA)                                      | [pdf](../f21/clients/trembath.pdf)        | [photo (jpg)](../f21/clients/trembath.jpg)               |
| β | Weaver, Cowart, Carey, Hubbard | <sup>**IP**</sup> <sup>**iOS**</sup> Pelvic Floor Smart Wand - Mobile App                      | [link](#weaver)          | [YouTube](https://youtu.be/1UZwzV1hSJI)                                      | [pdf](../f21/clients/weaver.pdf)          | [pitch (pptx)](../f21/clients/weaver.pptx)               |
| γ | Gopalakrishna                  | <sup>**IP**</sup> Boomcaster                                                                   | [link](#gopalakrishna)   | [YouTube](https://youtu.be/JnwpO7XuMpk)                                      | [pdf](../f21/clients/gopalakrishna.pdf)   | -                                                        |


<a name="brumfield"></a>

#### A. Amanda Brumfield - Honors Carolina Database For Resumes & Beyond

Honors Carolina needs a browser-based tool that would give students an easy way
to sort through a large volume of document samples to find the ones most
relevant and helpful to them. The initial use case would be a resume database,
but this could grow to any type of document/file, such as cover letters and
personal statements.

<a name="clark"></a>

#### B. Rebecca Clark, Matt Verber - <sup>**iOS**</sup> Sweepstat Mobile App

_Instructor note: this project has an iOS development component, so at least one
team member will need a Mac._

We are developing a mobile app, for both Android and Apple devices, that can be
used to interface with a portable potentiostat (the instrument used to make
electrochemical measurements) via Bluetooth. We have worked previously with
teams from this course and have made great progress so far! So we are looking to
continue working with a new team to wrap up some finishing touches on previous
and add additional features. We are mainly looking to improve/finish the GUI,
add automated analysis, add the ability to upload/view cloud data, and
evaluation/remediation of instrumental noise.

<a name="maddox"></a>

#### C. Amy Maddox - Strength^n, for meaningful connections

people crave meaningful connection to something larger than themselves. being
part of an organization has the potential to satisfy that need, but often we
have limited connections. Strength^n (strength to the power of "n") is a
randomization algorithm that allows an administrator to make combinations from a
list of participants, at random, and avoiding previous combinations.
participants can be classified, and combinations can be make within or among
classifications. the administrator tool works well, but there is no user
interface other than an email announcing your new combination. the goal of this
project is to create a user interface to facilitate scheduling with your new
combination, visualizing your growing network of meaningful connections, and
maybe even getting conversation sparks. come and help make this simple yet
powerful community-building tool even better!

<a name="toben"></a>

#### D. Daniel Toben, Ildar Sagdejev - Litter Map GIS Rendering

Plastic pollution is increasing exponentially, and it is a really big problem.
The future of humanity involves cleaning up our waste. Help an environmentalist
that has cleaned up over a million and a half pieces of plastic pollution add a
data science feature to their website under development! Build an engine in any
language to pull data from Open Street Maps, perform a logic, and output data in
any format, then render that output in ArcGIS/Google as a Heat Map. This heat
map will eventually be a predictive overlay on the the website to show locations
that likely have a littering/plastic pollution problem. Your work will be fully
incorporated into the website, and you will be credited as you please!

A bonus if your output can be a ready to go raster object that can be added to
our Leaflet JS widget immediately.

The website we are developing will be a one-stop-shop for people interested in
plastic pollution mapping and cleanup. In short, it lets people report locations
that have litter/pollution, Local municipalities are automatically updated, and
a global map is formed. People who want to volunteer can share their cleanups on
social media, and predictive litter mapping can become possible. We believe that
the heart of environmentalism needs to be developing a caring heart for the
planet, and a primary way of doing that is through acts of service.

The project here specifically is to collect key:value pairs from Open Street
maps, perform a logic (can provide or you can leave it flexible), use proximity
analysis in GIS, to output data is a raster object. imagine like a weather maps
radar or a GIS heat map layer. It will be a semi-transparent layer that will go
over our global map that helps to predict where pollution is. Look forward to
your help!

<a name="barron"></a>

#### E. Willie Barron, Ryan McHargue, Harvey Duperier - <sup>**IP**</sup> <sup>**iOS-only**</sup> WAVE

_Instructor note: this project requires signing an IP agreement._

_Instructor note: this is an iOS-only project, so all team members will need a
Mac._

WAVE is a nightlife-centered network of technologies that optimize both the bar
owner and bar goer experiences. This project will focus on the development of
the bar owner-facing side of the network, integrating data and analytics to
augment bars’ marketing efforts.

<a name="simon"></a>

#### F. Matthew Simon - Collect SMART

Regional and state level data commonly exists but high-quality primary community
level data is hard to find. Community level data is needed to unmask the
disparities within a community. Reliable high-quality primary data is needed to
help assess community needs. Validated community sampling methods are needed to
collect accurate, high quality primary data that don’t require extensive
expertise in population sampling, geographic information systems and costly
software and computer equipment. Many local health departments do not have the
resources to collect this type of data and lack sampling expertise. Thus,
Collect SMART would help aid health departments and other community-based
organization in collecting high quality data at a low cost.

Collect SMART allows for the rapid collection of community-level data. It
improves the timeliness of data collection while reducing the cost of purchasing
mapping/data collection software and equipment. The beta version of the ap and
website have been tested in 3500 interviews in five states. It has been proven
to be an efficient and cost-effective solution for obtaining high quality,
representative community health data. The software can be used by hospitals,
local health departments, emergency managers or any other community-based
organization to collect information about the status of their community. Collect
SMART has two parts; the project management dashboard website and the mobile app

The web application uses U.S. Census api to determine the population in an area
and implement a random sample with probability proportional to size. Once a
sample is drawn, the application allows the project manager to push the sample
out to the app, directing survey teams to the appropriate address to conduct an
interview. The web application then allows a project manager to monitor the
progress of surveyors and analyze incoming data from the field. The project
dashboards allows the manager to see the number of surveys completed by each
team and by sampling unit. A separate desktop application can be used to design
the questionnaire and export an .xml file which can be uploaded through the web
application and pushed out to the app.

The mobile app was built on the open-source framework of Epi Info, a free
statistical software/platform developed by the CDC. The mobile app adds a
mapping module which allows the project manager to upload spatial data, such as
census blocks or predetermined survey locations. The mobile app takes
questionnaires built in the desktop version of Epi Info, allows users to
navigate to the survey locations and collect responses. It is compatible with
Android tablets and phones. With electronic devices data can be collected easily
and with fewer errors. To ensure data security, a cloud-based database is used
for survey responses. Along with the features of collecting data, the app will
be able to communicate with other teams in the field and the project manager.

<a name="stone"></a>

#### G. Jennifer Stone - <sup>**IP**</sup> BigWords

_Instructor note: this project requires signing an IP agreement._

Billions of dollars are spent to remediate the 'skills gaps' in our education
systems. The gaps are preventable through daily read aloud practices. But
parents either don't know or are overwhelmed by the demands of modern life.
Physicians want parents to read more and hand them paper book logs to fill out.
Parents need a tool to help focus caregiving teams on the one thing science
shows babies' brains need most: words.

BigWords is a FitBit for words. It provides caregiving teams a communication
platform and feedback on the quantity and quality of words babies hear in
storybook reading. Additionally, BigWords provides personalized word-based book
recommendations to keep the word collections growing. BigWords grows big brains.
BigWords is growing the innovators of the future, one word at a time.

<a name="radsky"></a>

#### H. Vitaly Radsky, Rocky Moon - Easy Access Counselor Portal

Easy Access is an early stage social good start-up building college search and
advising tools to help low-income and first generation students access college.
The product that we are currently building is a caseload management tool for
school counselors working with high school juniors and seniors. Based on our
experience in schools, high school counselors often lack any form of caseload
management. They do not track meetings with students, and often don’t have a
good idea of what each student is planning to do after high school. With
caseloads of 200-400 students, many students fall through the cracks.

Over the past two years we have worked with CS for Social Good, Summer of Code,
and two semesters of CS523 to build a prototype counselor portal that allows
high school counselors to manage their students. We are looking to build on top
of the existing product by adding features including: the ability for multiple
counselors to edit and view a cohort of students, a web-based college search
interface, and refining the data upload and download process.

<a name="halpin"></a>

#### I. Peter Halpin - Online Peer Learning for Intro Stats

This project is about extending web apps built using
[R-shiny](https://shiny.rstudio.com) to include collaborative features, such as
those available via convergence.io (a JS library). I am pretty wedded to R-shiny
for content development, but am flexible in terms of how to "collaborify" the
content -- convergence seems promising but there may be better options.

<a name="mcdaniel"></a>

#### J. Dezbee McDaniel, Graham Watkins - <sup>**IP**</sup> Health Equity Project (saving lives!!)

CliniSpan Health is a company that is helping POC, women, and people in rural
areas get access to clinical research. The access to research then creates more
efficient medicines for those particular groups. These groups take improper
dosage amounts, see inadvertent side effects, and generally react more
negatively to some medicines because enough people of that group were not
involved in initial testing. Think of how this might affect your mom or
grandmother. You can help on the journey to create medicines that make their
health states much better! This software project will help to effectively reach
and engage those communities of people in order to achieve better healthcare
outcomes for them.

<a name="hu"></a>

#### K. Brian Hu, Paul Tunison - XAITK: The Explainable AI Toolkit

We are developing an open-source explainable AI toolkit (XAITK), which serves as
a collection of resources and tools for explaining and interpreting "black-box"
machine learning models. Specifically, we have created the xaitk-saliency Python
package, which generates visual saliency maps to help reveal what a model pays
attention to when it makes its predictions (e.g. for image classification,
object detection, etc.)

We have so far developed an API in terms of Python classes and functions. In
addition to this, we want to have a web API to expose algorithm configuration
and operation for over-the-web scenarios like cloud computing. We envision this
web service being containerized to standardize the application environment as
well as to be compatible with various cloud frameworks. An additional service
that provides a web UI for interacting with this “backend” service, provided as
a separate server and container, would also be of use as a demonstration example
of both algorithm functionality as well as integrating with the backend service.

You can learn more about the overall effort at the following website:
[xaitk.org](https://xaitk.org)

<a name="shah"></a>

#### L. Neal Shah, Gavry Eshet, Autumn Konz - <sup>**IP**</sup> Personalized Reminiscence Therapy

_Instructor note: this project requires signing an IP agreement._

Our mission at CareYaya is to utilize our skills and energy, to make innovations
to improve the quality of life for your grandparents and people like them.

Goal of the project is to deliver Personalized Reminiscence Therapy through a
mobile app, to create a valuable method of engaging senior citizens, help reduce
feelings of isolation and depression, and improve life satisfaction.

If we design it well and store the data from the reminiscences, others in the
family may enjoy learning things about their parent or grandparent that they
didn’t even know before!

And the memories could be preserved for the future, in a journal electronically
that the family can keep and look back on.

A real moonshot goal, depending on your skills and how well we develop this,
would be to create a digital story of the person's life. This digital story can
live on even after one day the elderly person passes away, so that the family
can have something to remember them by.

<a name="barber"></a>

#### M. Brennan Barber - HOLIND: Mobile app to facilitate at home treatment for disabilities

HOLIND is a product designed to capture therapist treatment recommendations and
patient progress data.

It eliminates the need to take/receive handwritten instructions for at home
therapy and allows the patient to easily record the progress made between
appointments in the clinical setting. Often times appointments therapist
instructions can be lost in translation, or simply lost! This will be an easy to
access platform that captures all data in an accessible way for patients and
providers.

<a name="gordon-larsen"></a>

#### O. Penny Gordon-Larsen, Alexia Kelley - Software for Research Impact

In a partnership between Gillings and the Health Sciences Library Informatics
team, we propose a demonstration project for generating tools for showcasing
UNC’s research impact. We propose to begin with a model for Gillings that could
be adapted for broader campus strategies.

For this demonstration project, we are seeking ways to show the impact of the
research being conducted by Gillings School of Global Public Health researchers.
We have a complex strategy in place with programming already developed and
implemented using the SCOPUS literature citation database
[https://www.scopus.com/home.uri](https://www.scopus.com/home.uri). Our
programming works excellently for point-in-time data analysis.

However, we are in great need for developing an API query and results collection
system to automate and turbocharge these searches, store the results on Gillings
database infrastructure, and allow us to very quickly tap the latest publication
information in the SCOPUS database for ongoing impact analyses. The priority for
this class project would be, in order:

1. Data retrieval (through API)
2. Data storage (‘locally’)
3. Data analysis/visualization

<a name="yu"></a>

#### P. Yichen Yu, JoJo Yang - <sup>**IP**</sup> Motivation

_Instructor note: this project requires signing an IP agreement._

Motivation is a habit tracker app that helps students keep up with their daily
habits and homework. It also serves as a platform for students with the same
habits to connect with each other and find habit buddies. We have done
pre-research, provide all design panels, and also got usability testing results.
So all you need is to do the coding part! Most importantly, we have worked with
COMP 523 before and we are also students, we understand you the best!

<a name="hurlbert"></a>

#### Q. Allen Hurlbert - Avian Diet Database

At the core, this project consists of building a web-based data entry portal.
Some of the required features include:

- login/user accounts
- automated QA/QC checks, some of which will interface with external APIs
- a reviewer dashboard to easily see any issues raised by the QA/QC checks
- integration of entered and approved data to an existing database on Carolina
  Cloud Apps (which can be explored at aviandiet.unc.edu)

<a name="walls"></a>

#### R. Taylor Walls - FANzPlay...The Game within a Game!

FANzPlay is an early stage sports and entertainment app that will allow FANs of
the most historic rivalries in sports to play against each other LIVE on Game
Day to Earn their OWN BRAGGING RIGHTS!

FANS will compete in timed, head to head, sport specific trivia for the
opportunity to WIN the gifts, prizes and services being offered by our sponsors
AND trash talk! Those sponsors would be given an opportunity to learn data rich
analytics on the lifestyles and loyalties of the fans that played the FANzPlay
App

FANzPlay will engage fans against the fans they “love to hate” in the
competitive match ups to see which fans know the most about the
teams,/conference, etc. As we scale, FanzPlay is available for in arena ( HD/
Jumbo Tron Integration )+ out of arena fans!

We understand the pandemic changed the wide world of sports and our focus is to
return to the arena or at home with the fans as We Change the way we Play!

If YOU could compete against the FANS you LOVE to hate, would you PLAY?

FANzPlay...The Game within a Game

<a name="gsell"></a>

#### S. Christopher Gsell - Cafe Reservation System

When cafes get crowded, especially in Chapel Hill and especially with the
limited Covid seating, it becomes difficult to find a table. Moreover, many
people love to work for long periods of time at a cafe, especially with remote
work still prevalent. While it may be ideal for the customer to buy a cup of
coffee and sit at the table for hours, it is not ideal for the cafe. This app
will solve this issue by allowing customers to reserve tables in advance. If a
cafe is particularly busy during a part of the day, they can even charge for
these reservations. This app will be a win-win as customers will be assured a
table if they reserve it, and cafes will make money from those who before were
taking up space.

<a name="harris"></a>

#### T. Talpha Harris - Augmented Interactive Education Platform

Ancient Path Adventures is a creative ministry. Our project will combine bible
stories with general education topics in an Augmented Interactive Education
Platform. Elementary students can enhance their learning journey through
augmented books that feature AR components and QR codes. The QR code will lead
to the web-app which features: Module quizzes associated with student accounts
and leaderboards. Facilitators can track student progress, assign new modules
and award students through the platform.

<a name="steelman"></a>

#### U. Tyler Steelman - ZIP Code Lookup Tool

Researchers attempting to display a survey participant's congressional district
are presently limited in their ability to do so. Asking individual their
congressional district is rife with errors as many people do not know the
district they live in. One method is to ask a user their ZIP code (which is much
more memorable) and then display the corresponding congressional district to the
user. However, programming each ZIP code-district pair is time and cost
prohibitive. This project will design a web tool that makes this data available
as a database that can be queried for use in survey software. Users will provide
a spreadsheet of original data (ZIP code-district pairs) that will be hosted and
accessible through a URL that sends ZIP code data to the web tool and sends back
the ZIP code's corresponding congressional district.

<a name="joseph-nicholas"></a>

#### V. Tessa Joseph-Nicholas - Online Training for CS ULAs

Help us develop a customizable online training for ULAs (Undergraduate Learning
Assistants) in the Computer Science department!

<a name="maitland"></a>

#### W. Scott Maitland - Where's the Play?

I want to gamify teaching little leaguers where they are supposed to throw the
ball based on different game scenarios. Ie. Runners on 1st and Second and ball
is hit to you in right field. Where do you throw the ball?

<a name="king"></a>

#### Z. Steven King, Max Hudnell, Daniel Sanchez - VCP Volumetric Capture Processer App

VCP produces volumetric 3D models from multiple video and data sources.

This project will build the public-facing interface to a computer vision
volumetric capture project being built by the Reese Innovation Lab.

<a name="trembath"></a>

#### α. Dimitri Trembath, Paige Hornsby, Laura Stadnik - No More Indecipherable Doctor Handwriting

Lab oversight is a major responsibility for many doctors, particularly
pathologists. Despite an increasing reliance on electronic health records for
charting patient visits, etc., too much administrative time for physicians is
still spent filling out paper forms. Many of these forms, particular in the area
of lab administration, are a repetitive process of reviewing/checking boxes
followed by signature and dates that attest that the appropriate person has
verified certain results. Creating an online version of these forms that can be
easily accessed by both laboratory staff and the appropriate
physician/administrator will allow for an easier and more seamless route of
communication and more robust record keeping required for standard laboratory
inspections.

<a name="weaver"></a>

#### β. Michael Weaver, Delaney Cowart, Erin Carey, Devin Hubbard - <sup>**IP**</sup> <sup>**iOS**</sup> Pelvic Floor Smart Wand - Mobile App

_Instructor note: this project requires signing an IP agreement._

_Instructor note: this project has a probable iOS development component, so at
least one team member may need a Mac. It's possible that the project can be
completed without a Mac, but that's not clear at this point._

Our project entails building a mobile app that will work in tandem with a smart
wand to relieve the symptoms of pelvic floor dysfunction. Pelvic floor
dysfunction is an extremely prevalent disease, and we hope that the app will
help to objectify the therapy process, allowing for treatment to become much
more accessible... helping millions of people around the world. This project is
submitted by TorchTech Industries, a team that began as a group of seniors in
the UNC class of 2021. Thank you for your interest, and we are eager to begin
working with your team!

<a name="gopalakrishna"></a>

#### γ. Hari Gopalakrishna - <sup>**IP**</sup> <sup>**iOS**</sup> Boomcaster

_Instructor note: this project requires signing an IP agreement._

_Instructor note: this project has an iOS development component, so at least one
team member will need a Mac._

Develop a native mobile application for Boomcaster, a podcast recording web
application.

