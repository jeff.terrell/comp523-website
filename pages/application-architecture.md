---
title: Assignment 6: Application Architecture
stem: application-architecture
---

### Due

Monday, October 4<sup>th</sup> at 8am

### Context

Before you get started writing code, you need to make some decisions about the
overall architecture of your app and what technologies to use. These decisions
should be made thoughtfully. Much work will be done atop these foundational
choices. It is usually quite difficult, not to mention wasteful of time and
effort, to change your mind about such things later.

A few common examples of architecturally significant decisions include:

- Where and how to store data
- Which programming language(s) to use
- Which framework or major library to use

It can be challenging to know how to make these decisions. To provide some
helpful structure, you will be creating [architecture decision
records](https://github.com/joelparkerhenderson/architecture_decision_record),
or ADRs. There are various kinds of ADR &ldquo;schemas&rdquo;, but we will use a
lightweight one described below.

These ADRs, and this assignment's deadline, are useful as &ldquo;forcing
functions&rdquo; to make these kinds of decisions early so that other work can
begin. But they are also helpful records to refer back to later. It's not
uncommon to make a choice at one point, then regret that choice on a steep part
of its learning curve and be tempted to switch. Not that switching is always a
bad idea, but it's definitely not something to be done lightly, and having a
written rationale for that choice can serve to remind you of the benefits of the
original choice.

### Requirements

First, list the significant architectural decisions you will need to make for
your app. See &ldquo;Hints&rdquo; below for ideas.

Note that ADRs are intended for strategic rather than tactical things. You don't
need an ADR for which test runner library to use, for example. (Although it can
be valuable to capture a rationale for your choice in the corresponding git
commit message, as described
[here](https://github.com/RoleModel/BestPractices/blob/master/git/commit-messages.md).)

As a general rule, somewhere between 3 and 6 decisions is probably sufficient.

Then, for each decision, create an ADR with the following format.

0. **Summary** - A summary statement, in the form, &ldquo;In order to
   &lt;goal&gt;, we decided to &lt;decision&gt;.&rdquo; For example, &ldquo;In
   order to store data persistently, we decided to use PostgreSQL.&rdquo; I
   recommend writing this section last.

1. **Problem** - What problem do you need to solve? Describe the context of the
   problem. Why is it a problem? Why is it important to solve? Imagine writing
   to a classmate on another team. What would you need to tell them for them to
   understand the problem?

2. **Constraints** - Are there any kinds of possible solutions that are out of
   bounds for this project? For example, if the client isn't willing to pay for
   a solution, that's worth noting. Or perhaps solutions that only work on MacOS
   aren't something the whole team can work with. Capture the constraints, as
   well as the reason for the constraints, here. Also capture any assumptions
   you're making about what the client might want or not want.

3. **Options** - Which candidate solutions are you evaluating? List pros and
   cons for each one.

4. **Rationale** - Which option did you choose? Why?

Post your complete application architecture on your project web site.

### Hints

Here are a few common examples of significant architectural decisions that
should have an ADR describing the decision. Many of these decisions could be
made more than once for different components of the system, e.g. the frontend
vs. the backend.

- Which programming language should we use?
- Should we use a framework? If so, which one?
- Should we use a backend service (e.g.
  [Firebase](https://firebase.google.com/), [Parse](https://parseplatform.org/))
  or write our own?
- Where should our backend service be deployed to (e.g. Heroku, AWS, Azure)?
- Which database should we use?

I also welcome (but don't require) ADRs for processes rather than architectures.
Perhaps we could call those PDRs. A prominent example of a significant process
decision would be which git branching style to use, e.g.
[1](https://nvie.com/posts/a-successful-git-branching-model/),
[2](https://www.endoflineblog.com/oneflow-a-git-branching-model-and-workflow),
or (my favorite)
[3](https://github.com/RoleModel/BestPractices/blob/master/git/merge-strategies.md).
