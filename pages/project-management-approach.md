---
title: Assignment 2: Project Management Approach
stem: project-management-approach
---

### Due

Tuesday, September 7<sup>th</sup> at 8am (because Monday 9/6 is Labor Day)

### Context

Most professional software teams use a project management board to keep track of
tasks and coordinate their efforts. Developers and other stakeholders refer to
the board extensively throughout the project's development, and teams develop
habits and routines to keep the board up to date.

Many, many different tools exist for maintaining a project management board.
Some developers will argue about which tool is better the same way they might
about a programming language or a text editor. It's a subjective preference.

In the past, I required teams to use [Trello](https://trello.com) because it's
simple and free. However, based on student feedback, as of the fall 2021
semester, I'm now letting you decide what you think will work best for your
team.

You don't even have to use a classic project management board. You could simply
broadcast updates over email, or use a shared Google Doc or Google Sheet, etc.
But you must justify your choice; see below.

### Requirements

The primary deliverables are a document describing your team's approach to
project management and an implementation of that approach.

The document should be written to a hypothetical new developer joining your
project and must be thorough enough that the new developer would know:

- what the next thing to work on is (in a way that respects client priorities
  and avoids duplicate work)
- any decisions made (by the team, the client, or others) that are relevant to
  their current task
- which tasks are:
    - finished
    - waiting for client feedback
    - in progress
    - ready to work on
    - not ready to work on
- who worked on a finished task
- how to determine (at least loosely) whether the project is behind schedule

The document and the implementation should each be linked from your project web
site (i.e. two links total).
