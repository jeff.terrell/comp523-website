---
title: Assignment 8: Walking Skeleton
stem: walking-skeleton
---

### Due

Monday, October 18<sup>th</sup> at 8am

### Context

In most cases, projects involving more than one developer tend to accidentally
minimize communication and coordination. The easiest approach&mdash;at least at
the outset&mdash;is to partition up the tasks, work in isolation, and only
start to integrate the pieces when everything else is done.

This approach typically ends in disaster, especially when deadlines loom (like
the end of a semester). It's only then that misunderstandings surface. In some
cases, a large body of code was piled atop both sides of a misunderstanding,
and both sides are (understandably) reluctant to undo so much work.
Frustrations and hurt feelings abound, and the deadline slips. In the case of a
project like the ones in this course, in which there is no possibility of
extending the deadline, the product never comes together&mdash;sometimes
despite heroic last-minute efforts by the team.

The best way I've found to avoid these problems is to *integrate the pieces
first* rather than last. Misunderstandings can immediately be noticed and
addressed, before any investment of time or emotion can occur. Once integration
is finished, features that require further coordination between the pieces on
either side of the integration start from a place of everything demonstrably
working, which makes it much more difficult to deceive oneself about everything
being correct.

This is the goal of the *walking skeleton*: get the major pieces integrated,
even before the pieces are implemented. Integrate first.

In the term &ldquo;walking skeleton&rdquo;, both words are significant:

- **Skeleton** means that the result will not be a fully functional app, but
  just the &ldquo;bare bones&rdquo;, so to speak. Many features are ignored to
  focus on end-to-end connectivity (or head-to-toe, if you will).
- **Walking** means that the result does work. This isn't a clickable prototype
  of UI screens wired together in a fake way. Rather, this is something real,
  which actually moves data around.

Note that, at this stage, bugs and crashes are tolerated. We don't expect that
just anybody can come along and use your walking skeleton without experiencing
bugs. It might take you, as a sort of marionette, to use your knowledge of
limitations to avoid encountering bugs.

An important type of integration is integration with infrastructure; that is,
*deployment*. [Heroku](https://heroku.com) makes this easy for most projects,
and the [App Lab](https://applab.unc.edu) can help as well.

### Requirements

Create a walking skeleton, as defined above. Deploy it in a way that makes
sense for your project:

- **Web apps** should be available via a URL to a user that does not have the
  source code nor any special software installed, but merely a web browser.
  Note that there will typically be frontend and backend components to a web
  app, and both will need to be deployed (typically to the same place).
- **Mobile apps** should be available to be used on mobile devices. If you're
  using React Native, I recommend (and permit, for purposes of this assignment)
  the use of Expo to let others experience your app; in other words, you can
  assume a user has the Expo app installed on their phone. Or, you could use
  Expo's [Snack](https://snack.expo.io/) to publish your code to the web so
  that users can run your code on an emulated mobile device via their browser.
  Or you can
  [publish](https://docs.expo.io/versions/v32.0.0/workflow/publishing/) your
  Expo app to the web directly. Or if you're developing an iOS app, you can use
  [TestFlight](https://developer.apple.com/testflight/) to let others
  experience it. Note that, unless you use a hosted backend service like
  Firebase, you will also have to deploy the backend somewhere.

Note that you may omit the authentication piece from the walking skeleton.

Post a link to your deployed walking skeleton on your project web site.

### Hints

This assignment can be pretty painful! I believe the pain is worth it, for the
reasons outlined above&mdash;and if you skip this step on a future project, you
might learn that the pain you feel later in the project is greater than the
pain of the walking skeleton.

I strongly recommend starting as early as possible and taking advantage of
other resources available to you, such as the [App
Lab](https://applab.unc.edu).

This assignment will likely involve reading a fair bit of documentation of
tools that other people have written, and that you're not familiar with. If
you're (understandably) in skimming mode, trying to sift through lots of text
to find what's relevant to a problem that you have, but not having much luck,
consider slowing down and blocking off an hour or three to read more carefully
and try to more deeply understand the underlying concepts and boundaries.
