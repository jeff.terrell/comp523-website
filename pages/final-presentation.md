---
title: Assignment 16: Final Presentation
stem: final-presentation
---

### Due

During the final exam period (Saturday, December 4, 12–3pm).

I will randomly order the teams, and all will present during the 3 hour period.

### Context

Show others how your project ended up, and get practice presenting. For better
or worse, it's usually not enough to know something or to have done something;
you have to also be able to talk about it and show people.

### Requirements

Prepare an **7-minute max** presentation with the following outline:

1. **Project overview** - What problem is your project solving? Who are the
   users?
2. **Architecture** - Show us your architecture diagram and tell us what
   language(s), tools, and frameworks you're using.
3. **Lessons learned** - What will you do differently on your next
   collaborative software project?
4. **Demo** - Show off your work! Walk us through how a user will use it.

Each team member should participate in both the preparation and delivery of the
presentation.

### Hints

**Rehearse** as a team to get the timing right. As little as 4 minutes is fine,
but try not to go over the 7-minute mark. (We're trying to get through 26 teams
in 3 hours, including swap time.) When I give talks, I sometimes rehearse more
than 10 times to really get it down. Not that you have to do that, but it can
make the difference between a decent presentation and a great one, if you're
feeling ambitious.
