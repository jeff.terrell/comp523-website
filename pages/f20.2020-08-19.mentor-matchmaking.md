---
title: Fall '20 Lecture Notes - Wed Aug 19 - Mentor Matchmaking
stem: f20.2020-08-19.mentor-matchmaking
---

Today was a bit of a dumpster fire. I won't post a recording, because it would
be of little value. But I'll summarize what little we accomplished below.

### Announcements

- I added a Piazza site; you should have been added and notified.
- I have office hours available now on Tuesdays from 10a-12p. (See [my UNC
  site](http://terrell.web.unc.edu/) for a signup link.)
- Is everybody OK with me posting recordings on YouTube? In general, I like for
  others to be able to benefit from my lectures. And I try to be careful not to
  compromise student privacy. Your videos are off, and I only ever say first
  names (and no names for private chat messages). But if you're uncomfortable
  with that, let me know.
- If you're moving out of a dorm in the next week and need to miss a lecture,
  let me know&mdash;that'll be excused.
- Remember that [Assignment 1: Project Management
  Board](../project-management-approach/) is due Sunday (soft deadline) or by the
  time you meet with your mentor (hard deadline).

### Teams/Projects have been assigned!

- you can see them on [the projects page](../projects/)
- most people got a high preference
- lowest project-priority match: 1 submission got their 4th preference (2x 3rd, 2x 2nd, 12x 1st)
- no teams were broken up for team-priority preferences
- lowest team-priority match: 1 submission got their 8th preference (1x 7th, 1x 5th, 3x 4th, 3x 3rd, 3x 2nd, 4x 1st)
- to reduce the possibility of my unconscious bias affecting assignments, I didn't see names of projects, clients, or students when matchmaking

### Mentor matchmaking

For most of the rest of lecture, everybody was in a breakout room with their
team.

The goal was to get one submission per team of which mentor times the team was
available for. When I had all of that data, I could assign teams to mentors.

### Problems

- There were some technical difficulties with the meeting. I changed the
  settings so that students could join only with an @email.unc.edu address.
  (Before, it was any \*.unc.edu address.) I did this because I wanted to use
  pre-assigned breakout rooms (one per team), and apparently that was the way to
  do it. I hoped this wouldn't affect anybody, but it did, and we spent 15
  minutes wrestling with it, trying to get everybody in.
- It took longer than expected to get the last team submission in. All teams but
  one were reasonably fast, but 1 team was significantly longer, and I couldn't
  start scheduling until I had all the data.
- The actual scheduling took longer than expected. I had some automation
  techniques in place (including an availability matrix in Google Sheets), but
  other parts of the process were manual and slow.
- I finished scheduling but there were still 2 teams unscheduled. I forgot that
  we are currently short by 2 mentors.

### Next up

- We'll talk about project management, like I wanted to do during lecture.
