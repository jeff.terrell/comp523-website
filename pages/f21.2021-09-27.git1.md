---
title: Fall '21 Lecture Notes - Mon Sep 27 - Git, part 1
stem: f21.2021-09-27.git1
---

- slides in [self-contained HTML](../f21/lectures/2021-09-27.git1/slides.html)
- video [on YouTube](https://youtu.be/H4qL-2sHhAk)
- [transcript of video](../f21/lectures/2021-09-27.git1/transcript.vtt)
