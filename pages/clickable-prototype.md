---
title: Assignment 4: Clickable Prototype
stem: clickable-prototype
---

### Due

Sunday, September 20<sup>th</sup> at 8am

### Context

A clickable prototype starts with a set of images defining what the various
screens of a finished app will look like. Then the screen images are linked
together by making regions of them that correspond to UI elements like buttons
clickable, where clicking on them takes you to another screen. In this way, you
can create the clickable prototype, which is not a fully functional app, but
feels remarkably like one, at least on a certain constrained path.

The clickable prototype is useful for two purposes.

First, designs are a very valuable communication device between the development
team and the client. Recall that the client is the one with the original vision
of the software. Such a vision cannot be communicated directly from your
client's head into yours, but has to be communicated imprecisely through words.
To arrive at an app that faithfully captures your client's vision, you will
probably need to _iterate_: propose something specific to the client, elicit
feedback from your client, and incorporate that feedback into the designs. Most
of the time, you will not get it exactly right on the first design. That is
expected and entirely OK. The important thing is to create something specific
for the client to provide feedback on.

Second, designs are a useful synchronization device for the development team.
Everybody can have a shared sense of what the software will look like when it's
finished. Developers will need to make more decisions than what is represented
in the designs in order to capture the design of the underlying mechanisms.
But the UI designs do help.

### Requirements

The point of this assignment is to create a _draft_ clickable prototype. I do
not expect that you will fully satisfy your client on the first attempt. The
important thing is to get something _specific_ finished to serve as a
conversation starter.

Your designs should be full-color, rather than a low-fidelity
&ldquo;wireframe&rdquo; produced by a tool like Balsamiq.

As for scope, include all the screens necessary to cover the
&ldquo;need-to-have&rdquo; features, as specified by your [user
stories](/user-stories). I expect that, for most apps, you will need at least
half a dozen or so screen images to create a clickable prototype that covers
these features. Note, however, that many of these screen images will likely be
only slightly changed from others; for example, one image might be for a blank
login screen, and the other for a login screen that has an email address and
password typed in.

Include a link to your clickable prototype on your project web site. Ensure that
you can access the clickable prototype without being logged in to whatever site
it's hosted on.

### Hints

Remember the four design principles we covered in class:

- Proximity
- Alignment
- Repetition
- Consistency

As a UNC student, you have access to Adobe XD, which is capable of making
screens and clickable prototypes.

If you create screens with some other tool (like Adobe Photoshop), you can
create a clickable prototype with a tool like [Marvel](https://marvelapp.com/).

Many designers are now using Figma, a web app, to create their designs and
clickable prototypes. It's like Google Docs: you can collaborate in real-time on
a design with your friends, without installing any software.
