---
title: Fall '21 Lecture Notes - Mon Oct 25 - Testing, part 2
stem: f21.2021-10-25.testing2
---

- slides [in self-contained HTML](../f21/lectures/2021-10-25.testing2/slides.html)
- video [on YouTube](https://youtu.be/xuicd_dSYHo)
- [transcript of video](../f21/lectures/2021-10-25.testing2/transcript.vtt)
- [commits in the testing tutorial repo](https://gitlab.com/jeff.terrell/testing-tutorial/-/commits/master) (a thorough write-up of what I showed during the testing tutorial, including important concepts)
