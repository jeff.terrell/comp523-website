---
title: Assignments
stem: assignments
---

Unless otherwise noted, assignments are due Monday morning at 8am.

Most deliverables are submitted by adding them to the web site that you will
create (Assignment 1). A couple are not suitable for publication to the web
site, in which case suitable delivery mechanism will be described in the
assignment details.

Starting November 1, I expect teams to finish not just whatever assignments are
due each week (most of which are pretty lightweight), but also to work on
adding features, and to document each new feature with a [demo
video](../demo-videos/), which should be linked from the journal on their web
site.

<div class="assignments-table">

|  # | assignment                                                     | due                                                     | delivery | difficulty |
|----|----------------------------------------------------------------|---------------------------------------------------------|----------|------------|
|  0 | [project preferences](../project-preferences/)                 | **Friday** Aug 27 (by 8 AM)                             | email    | ☕         |
|  1 | [web site](../web-site/)                                       | **Tuesday** Sep 7 (by 8 AM)                             | web      | ☕☕       |
|  2 | [project management approach](../project-management-approach/) | **Tuesday** Sep 7 (by 8 AM)                             | web      | ☕         |
|  3 | [user stories](../user-stories/)                               | Sep 13                                                  | web site | ☕☕       |
|  4 | [clickable prototype](../clickable-prototype/)                 | Sep 20                                                  | web site | ☕☕☕☕   |
|  5 | [APPLES reflection 1](../apples-reflection-1/)                 | Sep 27                                                  | web site | ☕         |
|  6 | [application architecture](../application-architecture/)       | Oct 4                                                   | web site | ☕☕☕     |
|  7 | [architecture diagram](../architecture-diagram/)               | Oct 4                                                   | web site | ☕         |
|  8 | [walking skeleton](../walking-skeleton/)                       | Oct 18                                                  | web site | ☕☕☕☕☕ |
|  9 | [midterm presentation](../midterm-presentation/)               | Oct 18–20 (in class)                                    | web site | ☕☕       |
| 10 | [tech talk](../tech-talk/)                                     | Preferences due Oct 27; talk due Nov 1–Dec 1 (in class) | web site | ☕☕☕☕   |
| 11 | [test coverage report](../test-coverage-report/)               | Nov 8                                                   | web site | ☕☕       |
| 12 | [developer README](../developer-readme/)                       | Nov 15                                                  | web site | ☕☕       |
| 13 | [client-oriented overview](../client-overview/)                | Nov 22                                                  | web site | ☕☕       |
| 14 | [APPLES reflection 2](../apples-reflection-2/)                 | Nov 22                                                  | web site | ☕         |
| 15 | [personal report](../personal-report/)                         | **Wednesday** Dec 1 (before midnight)                   | email    | ☕         |
| 16 | [final presentation](../final-presentation/)                   | **Saturday** Dec 4 (during final exam period, 12–3pm)   | web site | ☕☕       |

</div>
