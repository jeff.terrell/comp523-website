---
title: Info for mentors
stem: mentors
---

This page describes an important part of COMP 523: mentors.

## Contents

- [The problem](#the-problem)
- [The solution](#the-solution)
- [The opportunity](#the-opportunity)
- [The mechanics](#the-mechanics)
- [The commitment](#the-commitment)
- [The application](#the-application)
- [FAQ](#faq)

## The problem

COMP 523 has a problem. It's a well-loved course, and many of our alumni say it
was their favorite at UNC. But our undergraduate enrollment has grown
enormously, from maybe 150 in 2009 to over 1,500 in 2018-19 (unofficial
numbers). Every year, we have more interest than capacity, so interested
students can't get in to the course. Some even graduate without ever getting
in.

One of the major bottlenecks in the course is the instructor, who not only
manages the lectures and overall course design but also weekly meetings with
each team. With very many teams, this can be overwhelming, even with help from
a teaching assistant.

## The solution

Last fall (2019), I piloted a new idea: bringing professional software
developers in as mentors to the teams. Each mentor would meet weekly with one
team, helping them overcome common challenges in collaborating with their
client and with each other. Based on feedback from students and mentors as well
as my own experience, I believe this idea worked very well, and I'm excited to
bring it back for the fall 2020 semester.

Professional software developers&mdash;even relatively inexperienced
ones&mdash; have overcome most of the problems the teams will face. This is,
after all, typically the students' first collaborative software project, and
the problems they face are typically fairly basic. And although a mentor is
welcome to get technical with the team, he or she is also welcome to keep the
focus on the collaboration aspects that most software developers can relate to
regardless of language or framework; students have other resources for
technical problems (like the [App Lab](https://applab.unc.edu)).

## The opportunity

Why should you care? For one thing, it's a way to give back. Did you have
somebody who helped you overcome these challenges? (Aren't you glad?) You could
be that person for some students. Seeing the impact your efforts have can be a
very rewarding experience.

For another thing, many teachers recognize that the best way to solidify your
knowledge about something is to teach it to others. Do you have a way of
collaborating on your team that's reasonably effective, but you'd like to
understand it better? Teach it to others, and you probably will.

## The mechanics

So what does a mentor do? Most importantly, a mentor meets weekly with their
team.  (Note: it's important to commit to these meetings and prioritize them,
despite work deadlines and other things.) These meetings can happen remotely
via video chat or in person (if the COVID-19 situation allows, that is), and
most students are able to get off campus and meet somewhere with more readily
available parking (although it might need to be on a bus line). A mentor also
fills out a form describing the team's progress and any issues discussed, so
that I have enough information to notice brewing issues and so that I can to
assign grades later (in a consistent fashion) despite not attending the
meetings myself. This form can set the agenda during the meeting, but you are
also welcome to structure the meeting however you like, so long as the form's
questions are answered.

## The commitment

How much of a commitment is this? I am accustomed to doing 30 minute meetings,
and that is totally acceptable. You may also take longer, if you prefer. The
form should take 15 minutes at most to fill out, and probably much less once
you're familiar with it. This is for the fall semester only, starting a couple
of weeks in and skipping fall break and Thanksgiving holiday as necessary (see
[the calendar](/calendar/)), so you'll have a total of 12&ndash;14 meetings.
Let's assume you spend an hour per week on this, for all 14 weeks. That's 14
hours. I suspect, by the end of the semester, you'll agree that the impact you
made on the students' professional lives will be well worth that much of your
time.

## The application

To apply to become a mentor, [apply here](https://www.tfaforms.com/4731293).
Please apply by the **deadline of Friday, August 28**.

## FAQ

- [What does a mentor do?](#job-description)
- [What are some problems a team might face?](#example-problems)
- [What if there's a problem I can't help with?](#escalating-problems)
- [I don't really do web apps or mobile apps. Is that OK?](#no-apps)
- [Is there any compensation for being a mentor?](#compensation)
- [Is this just a way to get free labor? Shouldn't you be hiring more professors and/or accepting fewer students instead?](#free-labor)
- [I can't or don't want to come to campus. Can meetings be remote?](#remote-meetings)
- [Can I meet outside of normal working hours?](#evenings-weekends)
- [What is the coding environment, or what languages and technologies are used?](#coding-environment)
- [How are mentors assigned to teams?](#matching-process)

(Do you have other questions? Please feel free to [email
me](mailto:terrell@cs.unc.edu).)

<a name="job-description" />

#### What does a mentor do?

The idea of a mentor is that they help a team navigate some of the complex
issues in software engineering. They should be somebody with some experience
doing software engineering.

A mentor meets with a team weekly, for 30&ndash;45 minutes, to discuss the
project and its progress. Specifically, mentors will lead a discussion that
typically includes things like:

- What did the team work on this week?
- How did the work go?
- What do the results look like?
- Did you meet your goals last week? If not, why not?
- What do you hope to accomplish next week?
- Are you having any problems with collaboration and teamwork, or with your
  client, or with your technologies, or anything else, or are you sensing that
  something could develop into a problem soon?

Finally, the mentor submits a brief, structured report to the instructor, to
keep him in the loop.

<a name="example-problems" />

#### What are some problems a team might face?

First, remember that, while you're welcome to get technical with a team, you
might not have experience with the language or framework they're using, and
they can get technical help [elsewhere](https://applab.unc.edu).

Of the non-technical problems, most of them are fairly basic. This is, after
all, the first collaborative project most students have worked on, and also the
first with a real client. Here are some example problems:

- How can we assign the work to be done, especially because some things can't
  start until other things are done?
- We told our client that we needed more information from them, but they still
  haven't responded. What do we do?
- How can we keep everybody working? Some people don't know as much about this
  part of the code, so it's hard for them to contribute.
- I think we should use this tool/language/framework, but my teammate thinks we
  should use this other tool/language/framework. How can we resolve our
  disagreement?
- When my teammate merged their feature, it broke my feature. Is this normal?
  What can we do to avoid this in the future?

<a name="escalating-problems" />

#### What if there's a problem I can't help with?

If it's technical in nature, refer it to the [App Lab](https://applab.unc.edu).
Otherwise, refer the team to me, and mention it in your report for the week. I
am available to help with problems that are too big or too thorny for you and
the team.

<a name="no-apps" />

#### I don't really do web apps or mobile apps. Is that OK?

Yes. As long as you have experience with software engineering, that is fine. If
students have difficulty with the technical aspects of their projects, they can
get help outside the mentor meetings, such as at the [UNC App
Lab](https://applab.unc.edu).

<a name="compensation" />

#### Is there any compensation for being a mentor?

No. (If a modest remuneration would be the deciding factor for you to get
involved, [I'd be interested to know it](mailto:terrell@cs.unc.edu) so that I
can have data to help me persuade administrative folks that it's worth the
accounting headache.)

<a name="free-labor" />

#### Is this just a way to get free labor? Shouldn't you be hiring more professors and/or accepting fewer students instead?

It's a fair question. First, some context about student enrollment. Our
department's undergraduate major enrollment has grown from about 150 ten years
ago to over 1,500 now (unofficial numbers). And we want to teach computer
science to whomever is interested to learn, even when it's hard to support so
many students, rather than turning people away. That said, at some point we may
have to limit the size of the major to prevent burning faculty out, and that's
something that is a discussion point in the department.

Second, although we're actively working to hire more professors, we're not the
only CS department with a lot of growth. There has been a surge of interest in
CS nationwide, and many CS departments are trying to hire more professors to
help deal with the growth. There aren't enough qualified candidates to go
around, and the process of getting approval for a position, inviting
applicants, and evaluating them is long, so it can literally take years to hire
people.

For this class, the good news is that it's pretty small (for us,
anyway&mdash;"only" 60 students are enrolled at this point). If we don't get
enough mentors, there's a fallback plan: I will meet with the teams myself.
This is fine, but the thought behind inviting industry involvement is that this
is a way to let students start to make connections with "real" software
developers and companies, and vice-versa. So although I could use the help,
it's more of an opportunity to enhance the class rather than an existential
need.

<a name="remote-meetings" />

#### I can't or don't want to come to campus. Can meetings be remote?

Absolutely. In fact, I recommend that meetings be virtual and remote until
there's a solution to COVID-19. In that case, having at least a couple
in-person meetings can help build rapport, especially at the beginning of your
relationship.

<a name="evenings-weekends" />

#### Can I meet outside of normal working hours?

Definitely! In fact, student teams often have many constraints during weekday
working hours because of classes, and many teams would prefer to meet on
evenings or weekends. You'll have a space in the application to list this
preference.

<a name="coding-environment" />

#### What is the coding environment, or what languages and technologies are used?

Each team decides which technologies to use, subject to the constraints of
their project. An early assignment is to make and justify a decision about the
technology stack they will use for their project. You can help them make this
decision, but please don't pressure them to use your favorite stack.

In general, I expect that most of what you can help the teams with transcends
particular tools and languages and deals more with the collaboration and human
side of software engineering. If the team has technical challenges, there are
resources such as [the App Lab](https://applab.unc.edu) that can help with
that.

<a name="matching-process" />

#### How are mentors assigned to teams?

The primary criterion is availability of both you and the team to meet at the
same time. So it's important to note accurate meeting times when you apply to
be a mentor, and to let me know before Friday, August 28 if those times change.

If there are other criteria that are important to you in matchmaking, feel free
to mention that in your application, and I will do what I can to accommodate
that, but no promises.
