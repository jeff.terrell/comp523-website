---
title: Assignment 13: Client Overview
stem: client-overview
---

### Due

Monday, November 22<sup>th</sup> at 8am.

### Context

In most cases, your client is not a developer. You need to give them a sense of
what the various pieces are, and what they need to know to manage it all in the
long run.

### Requirements

Create a page on your web site with the following information. Keep in mind that
the target audience for this page is your client. If your client is technical,
you are free to speak in a more technical way, but if so, please note for the
sake of your mentor and me that you're doing that deliberately rather than
ignorantly.

- What are the major pieces of your overall system? Include your architecture
  diagram (or perhaps a simplified version of it) and describe it in a way the
  client can understand.
- Where is each piece deployed?
- How can the client manage everything? (You should give your client admin
  access over the infrastructure, if you haven't already.)
- How much does everything cost? Is that subject to change? If so, under what
  conditions? Remember to include yearly subscriptions (like an Apple Developer
  membership) as well as monthly costs.
- Where does the code live? Is it open source or closed source? Does the client
  have a license to do what they want with it?
- How can the client access the live app?
- If the app isn't generally available, is there a way that the client can share
  the app with somebody else, so that they can use it?
- Of the &ldquo;need-to-have&rdquo; user stories (see [Assignment
  2](/user-stories/)), which ones are complete, partially complete, or
  unstarted?
- Did you make any progress on the &ldquo;nice-to-have&rdquo; user stories? If
  so, which ones, and how much progress?
- What do you think the highest priority next steps are?
