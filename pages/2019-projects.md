---
title: 2019 Projects
stem: 2019-projects
---

_These are the projects for fall 2019._ Some links may not work. You can also
see the projects for
[fall 2020](../2020-projects/) and
[fall 2021](../projects/).


### Contents

- [Project web sites](#project-web-sites)
- [Mentor assignments](#mentor-assignments)
- [Pitch recordings](#pitch-recordings)
- [Project details](#project-details)

### Project web sites

- [A (Ferris)](http://applys.web.unc.edu/) - Kwak, Chen, Shyu
- [B (Fruehwirth)](https://comp523teamb.web.unc.edu/) - Jacobson, Healy, Ren
- [C (Hinson)](https://www.cs.unc.edu/~joeson34/) - Wu, Hattle, Son
- [D (Bishop)](http://tarheelreader.web.unc.edu/) - T. Smith, Zhang, Fullwood
- [G (Kee)](https://frosty-tereshkova-9806e1.netlify.com/index.html) - Van Lith, Moore, Bateman, Goss
- [H (Cherner)](https://articleanalyzer.web.unc.edu/) - Ji, White, Thomas
- [K (Byrom)](http://comp523k.web.unc.edu/) - Harvey, Lee, Wang
- [L (Moon)](https://comp523easyaccess.web.unc.edu/) - Glontz, A. Smith, Frederick
- [M (Quimby)](http://undergradresearch.web.unc.edu/) - Braam, Higgens, Annunziata
- [N (Leisten)](http://teamn.web.unc.edu/) - Shrestha, Lu, Polo
- [P (Smith)](http://sanityscale.web.unc.edu/) - Ebbitt, Mayes, Kania
- [Q (Dick et al)](https://sweepstatapp.web.unc.edu/) - Xin, K. Liu, Skoczek
- [R (Burbridge)](http://comp523teamr.web.unc.edu/) - Schmidt, Dixon, T. Liu
- [S (Mahaney)](https://2wheeledtraveler.web.unc.edu/) - Madiraju, Truelove, Zhu

(I gathered some [fun facts](../posts/2019/09/06/projects-assigned/) about the
[project preferences](http://localhost:8081/project-preferences/) that fed the
project assignments.)

### Mentor assignments

| team | mentor      | meeting time (45 mins.) |
|------|-------------|-------------------------|
| A    | Franzen     | Wednesdays 5pm          |
| B    | Dinger      | Tuesdays 5:15pm         |
| C    | Yackenovich | Wednesdays 3:45pm       |
| D    | Farmer      | Thursdays 6pm           |
| G    | Lake        | Tuesdays 11am           |
| H    | Mowery      | Thursdays 2pm           |
| K    | Pollack     | Fridays 12pm            |
| L    | Brown       | Thursdays 10:15am       |
| M    | Vega        | Tuesdays 5:15pm         |
| N    | Nguyen      | Thursdays 12:30pm       |
| P    | Byzek       | Mondays 10am            |
| Q    | Blair       | Fridays 1:30pm          |
| R    | Wright      | Wednesdays 3pm          |
| S    | Auger       | Fridays 1:30pm          |

### Pitch recordings

- [Pitch slides for Monday, August 26 (PPTX)](../f19/lectures/pitch-day.2019-08-26.mon.pptx)
- [Recording from Monday, August 26 (on YouTube)](https://youtu.be/AcPO_o6FoOc)
- [Pitch slides for Wednesday, August 28 (PPTX)](../f19/lectures/pitch-day.2019-08-28.wed.pptx)
- [Recording from Wednesday, August 28 (on YouTube)](https://youtu.be/7aexz1E7BPA)

### Project details

Project titles and summaries are provided by clients.  Clients sensitive to
issues of intellectual property and confidential information will me marked
like so: <sup>IP</sup>.

- [A. Health self-management training for adolescents/young adults with chronic conditions (Maria Ferris)](#a.-health-self-management-training-for-adolescentsyoung-adults-with-chronic-conditions)
- [B.<sup>IP</sup> Software to Support Student Success (Jane Fruehwirth)](#b.-software-to-support-student-success)
- [C. Southern Legacies: The Descendants Project (Glenn Hinson)](#c.-southern-legacies-the-descendants-project)
- [D. Tar Heel Reader PWA (Gary Bishop)](#d.-tar-heel-reader-pwa)
- [G.<sup>IP</sup> SalonOn (Morgan Auriel Kee)](#g.-salonon)
- [H. Article Analyzer (Todd Cherner)](#h.-article-analyzer)
- [K. Simplified Music Notation (Stuart Byrom)](#k.-simplified-music-notation)
- [L.<sup>IP</sup> EasyAccess (Rocky Moon)](#l.-easyaccess)
- [M. Undergraduate Research Application portal (Boots Quimby)](#m.-undergraduate-research-application-portal)
- [N.<sup>IP</sup> UX Analytics (Max Leisten)](#n.-ux-analytics)
- [P. Sanity Scale (Tosha Smith)](#p.-sanity-scale)
- [Q. A Universal Platform for Personalized, Diagnostic Monitoring via Electrochemical Sensors (Jeffrey Dick, Matthew Glasscott, Collin McKinney, Matthew Verber)](#q.-a-universal-platform-for-personalized-diagnostic-monitoring-via-electrochemical-sensors)
- [R. Help Automate Operations and Logistics for a Medical Cleaning Company (Sir Robert Burbridge)](#r.-help-automate-operations-and-logistics-for-a-medical-cleaning-company)
- [S. 2 Wheeled Traveler (Jim Mahaney)](#s.-2-wheeled-traveler)

#### A. Health self-management training for adolescents/young adults with chronic conditions

Client: Maria Ferris, Director, Transition and Self-management Program, UNC
School of Medicine

Summary:

> The good news: Most adolescents and young adults (AYA) with chronic health
> conditions survive to adulthood!
>
> The bad news: Most AYA are not prepared to self-manage their health focused
> services and they transition from pediatric to adult-focused health services
> and most parents do not know when to let go of managing their children's
> health.
>
> Preparing the AYA to learn health self-management is a difficult task in the
> very short medical appointments. Even if AYA/parents go home with paper
> handouts, the material may not be appealing and therefore is not consulted or
> is lost.
>
> A patient and parent education curriculum on self-management (with educators
> input) has been created in print format, but data on how this material is
> used or consulted at home is not available.
>
> Our challenge is how to move this curriculum to a more attractive,
> interactive platform - an app- that is customized to patient/parent level of
> literacy and cognition.
>
> Case scenarios would be presented with video or perhaps as avatars, where the
> user would have to pick the correct answer. The user would then receive
> feedback based on their response.
>
> An example of a case scenario from our curriculum based at the
> www.med.unc.edu/transition website would be a 14 year old with a chronic
> condition that has mobility impairment (i.e.a wheel chair) and is given the
> task of organizing his/her medications in a weekly planner...the patient and
> parent would then receive a question, such as "is this appropriate at this
> age?"
>
> The answer is yes, if the patient has little or no cognitive impairment.
>
> For those with cognitive impairment, a modified scenario would occur: For
> example having the patient get the pill box weekly container out of the
> drawer.
>
> We would track baseline and follow up performance in our validated surveys
> for elf-management skills, to evaluate the outcome of our app (intervention).
>
> We would welcome your input, ideas and suggestions on our proposal. Thank you
> for your consideration
>
> PS: Our STARx program has been in existence for 13 years and we have been
> pioneers in most of our work. With over 70 publications and collaborators
> across the USA and a few other countries, we have been able to serve many
> patients/families.
>
> Our students of all levels have published their work with us and we encourage
> those who join our lab to consider publishing their work.

Materials:

- [Application (PDF)](../f19/clients/patient-transition-survey.maria-ferris.pdf)


#### B. Software to Support Student Success

Client: Jane Fruehwirth, Associate Professor, Department of Economics

Summary:

> Software designed to support student success, particularly in the process of
> writing an honors thesis, but potential is broader. Key aspects: timer to
> track writing/research, daily check-ins, ability to comment on check-ins and
> group chats

Materials:

- [Application (PDF)](../f19/clients/honors-thesis-task-tracker.jane-fruehwirth.pdf)


#### C. Southern Legacies: The Descendants Project

Client: [Glenn Hinson](https://anthropology.unc.edu/person/glenn-d-hinson/),
Professor, Department of Anthropology


Summary:

> The Descendants Project invites students to archivally explore the lives of
> lynching victims in N.C., and then to trace their descendants to the present
> day, using Census data, military records, cemetery listings, and more.  Our
> goal is to locate--and then interview--descendants of these victims, with an
> eye to engaging them, and the communities in which the murders happened, in
> public memorialization projects.  The difficulty is *finding* those
> descendants, and then keeping track of the crazily diverse set of archival
> sources that contribute to the creation of family trees.  That's why we're
> seeing your help--to develop a program that allows students to create
> schematic family trees, and to link important documents (e.g., marriage or
> death certificates) to those trees, creating a deep visual field that will
> not only facilitate the shared search for descendants, but that we can also
> present to the descendant families.  It's already been our experience that
> many of these families have asked us to fill in family histories that have
> long since been lost in those families.  Being able to present them with
> family trees, along with links to attached documents, would be a tiny step
> towards the kinds of reparations that ALL state institutions owe to
> communities that have been the historical  victims of racial terror.
>
> I'll add that keeping track of archival searches--and ordering them in a way
> that's visually accessible--has been a problem for the undergraduates engaged
> in this project.  With your help, the entire project of searching for, and
> interviewing, descendants, and of building relationships with communities
> engaged in public memorialization efforts, will be greatly facilitated.  Many
> thanks in advance for considering taking on this project!

Materials:

- [Application (PDF)](../f19/clients/descendants-family-tree.glenn-hinson.pdf)


#### D. Tar Heel Reader PWA

Client: Gary Bishop, Professor, Department of Computer Science

Summary:

> Tar Heel Reader is our extremely successful site with easy to read books for
> beginners of all ages. Over 1 million books are read each year all over the
> world.
>
> I'm trying to imagine how it will continue to work into the future when I am
> not around to support it. It is currently based on WordPress and needs
> constant updates to stay ahead of the bad people out there.
>
> One approach is to survive the way living things do, by multiplying. We could
> make it a static site with lots of small files and only relative links so
> that it could be simply hosted anywhere without configuration or the need for
> software updates. Then schools and groups could host their own copies.
>
> I've got a rough prototype working but there is lots to do. I'd like to see
> it become a PWA (progressive web app) that works efficiently and offline (we
> have a start on that too). We need to test that it will actually work when
> based on the kind of web infrastructure used by schools. And what browsers
> can we support?
>
> Could we enable users to easily add books to their local instance? Perhaps
> the books are only stored in their browser? Or on their Google Drive, or
> whatever. Can we enable them to share books with others?
>
> This will involve lots of front-end web technology.

Materials:

- [Application (PDF)](../f19/clients/tar-heel-reader-pwa.gary-bishop.pdf)


#### G. SalonOn

Client: Morgan Auriel Kee, Hairstylist

Summary:

> SalonOn will be a booking app that will allow salons to generate sales off of
> otherwise empty seats or booths. Clients will be able to sign up and be
> matched with stylists in the area who fit their hair needs. The two together
> are able to choose a salon location that works for them. I look forward to
> sharing more on my vision in class!

Materials:

- [Application (PDF)](../f19/clients/salon-booking.morgan-auriel-kee.pdf)


#### H. Article Analyzer

Client: Todd Cherner, Program Director, School of Education

Summary: A tool to support students in grades 3-12 with text annotation without
collecting email addresses.

Materials:

- [Application (PDF)](../f19/clients/text-analysis-tool.todd-cherner.pdf)


#### K. Simplified Music Notation

Client: Stuart Byrom (email:
[stuartbyrom@gmail.com](mailto:stuartbyrom@gmail.com))


Summary:

> What if instead of new students of music spending huge amounts of time
> learning how to read music, they spent that time practicing playing music?
> What if that made learning to play an instrument fun and not drudgery? What
> if that meant more people were able to experience the joy of playing music?
> This is all possible with a new notation that implements a What You See Is
> What You Play (WYSIWYP) approach.

Materials:

- [Application (PDF)](../f19/clients/music-notation-app.stuart-byrom.pdf)
- [Notation description](../f19/clients/music-notation-app.stuart-byrom.notation-description.docx)
- [App overview](../f19/clients/music-notation-app.stuart-byrom.app-overview.docx)
- [Notation comparison](../f19/clients/music-notation-app.stuart-byrom.notation-comparison.docx)
- [Stuart's bio](../f19/clients/music-notation-app.stuart-byrom.bio.docx)
- [Pitch video](https://youtu.be/wgDzEU1n-78)
- [Pitch slides](../f19/clients/music-notation-app.stuart-byrom.slides.pptx)


#### L. EasyAccess

Client: Rocky Moon, founder/entrepreneur

Summary:

> EasyAccess is an app and web-based platform the provides college-going
> resources to underrepresented students. High school counselors can track
> their students’ progress through the application and admission process.
> Post-secondary admissions counselors can use data generated through the
> platform to increase enrollment in students traditionally underrepresented at
> their institution. EasyAccess promotes equity in higher ed through:
>
> - Matching software that creates a Smart, Match, Reach school list unique to
>   each students needs and academic profile
> - Connection to the online application and communication with the admissions
>   staff of those schools
> - Digital admission essay editing 
> - Integrated educational tips so students can learn about the process as the
>   navigate each step
> - High school counselor back-end for tracking students’ progress through
>   the process and communicating with post-secondary admissions staff
> - College admissions staff data collection and communication with high school
>   counselors
>
> With many college-access programs out there, EasyAccess distinguishes itself
> as being the first all-in-one college access app for students, an intuitive
> and effective tracking software for counselors, and a data-rich resource for
> post-secondary institutions to increase enrollment of underrepresented
> populations. EasyAccess hopes to close the educational opportunity gap and
> achieve equity in the college admission process.

Materials:

- [Application (PDF)](../f19/clients/easy-access.rocky-moon.pdf)


#### M. Undergraduate Research Application portal

Client: Boots Quimby, Associate Director, Office of Undergraduate Research

Summary:

> To create an application portal for a variety of applications the Office for
> Undergraduate Research uses that will allow students to answer a set of
> questions, upload PDFs and add a faculty email to upload a letter of
> recommendation. The portal would then email the faculty to allow them to
> upload a letter of recommendation and then combine the PDFs uploaded by the
> student and the letter uploaded by the faculty. If possible, it would then be
> helpful if reviewers could be added to the portal that could review the
> materials.

Materials:

- [Application (PDF)](../f19/clients/application-platform.boots-quimby.pdf)


#### N. UX Analytics

Client: Max Leisten, CEO, Protopia

Summary:

> Dashboard to understand how students and alumni engage with the Protopia
> platform.

Materials:

- [Application (PDF)](../f19/clients/analytics-dashboard.max-leisten.pdf)


#### P. Sanity Scale

Client: Tosha Smith, CEO, Sanity Scale

Summary:

> At Sanity Scale, we are developing a "saner way to weigh" by developing a
> digital body scale that does not display your weight.  Instead, the scale
> silently pushes your data to an app on your smartphone.  We then do all the
> data crunching and contextualizing for you, and present your data to you just
> once a week, so that you can get the real, full picture.  In short, we help
> you track your weight without losing your mind.

Materials:

- [Application (PDF)](../f19/clients/sanity-scale.tosha-smith.pdf)


#### Q. A Universal Platform for Personalized, Diagnostic Monitoring via Electrochemical Sensors

Client: Jeffrey Dick, Professor, UNC Department of Chemistry, with Matthew Glasscott, Collin McKinney, and Matthew Verber

Summary:

> Our goal is to ensure all people have access to highly sensitive
> measurements. Electrochemistry stands poised to provide a robust,
> inexpensive, and reliable framework for personalized medicine and diagnostics
> as well as environmental monitoring. Our call to action is meant to solicit
> aid in developing an application for iOS and Android devices that communicate
> with a state-of-the-art, inexpensive (&lt;$50) diagnostic device developed by
> our laboratory.

Materials:

- [Application (PDF)](../f19/clients/sweepstat-controller.jeffrey-dick.pdf)
- [Additional materials (ZIP)](../f19/clients/sweepstat-controller.jeffrey-dick.additional-materials.zip)


#### R. Help Automate Operations and Logistics for a Medical Cleaning Company

Client: Sir Robert Burbridge, Owner/Manager, OnSite Drapery Cleaning, LLC

Summary:

> Work with an experienced software entrepreneur and project manager to build
> real-world experience in a low-stress opportunity.
>
> OnSite Drapery Cleaning provides medical-grade cleaning of hard-to-clean
> items to area clinics and hospitals.  We make sure the hospital curtains,
> lobbies, staff rooms, and more stay clean and safe for all visitors.
>
> Our process is largely manual and has a variety of elements that can be
> automated to reduce errors, improve customer experience, and generally
> streamline business activity.  The project lead is a serial entrepreneur who
> recently purchased the company and is working to modernize and improve every
> aspect of it to provide top-notch services to clients.
>
> The list of available tasks is intentionally larger than we will be able to
> accomplish in the course of the program.  This allows us to choose tasks
> based upon the *skills* and *interests* of the participating students.  We
> will select work that helps students use their current knowledge and grow
> their skills.  By having a number of available objectives, there's a
> low-pressure environment designed to accommodate various student backgrounds
> and interests.
>
> We will be working with git (through BitBucket) and creating software that
> connects to existing APIs (such as Google's Business Suite... Gmail,
> Calendar, etc.), Wordpress plugins for our marketing site, automation tasks
> for production of client-facing documents, and more.
>
> The project lead has direct experience with all the technologies involved,
> and is happy to work with students as much or as little as needed to ensure
> success.
>
> Thanks!

Materials:

- [Application (PDF)](../f19/clients/drapery-cleaning-tools.sir-robert-burbridge.pdf)


#### S. 2 Wheeled Traveler

Client: Jim Mahaney, Senior Research Associate, Department of Computer Science

Summary: Just like automobile drivers, vintage motorcyclists have come to rely
on cell phones more and more while riding their motorcycles.  Although there
are a number of useful apps available for riders, changing between apps while
riding is both difficult and dangerous.  2 Wheeled Traveler eliminates the need
for multiple apps by combining features like speed, weather and where to find
the nearest gas station onto a single screen that overlays your standard
mapping program.

Materials:

- [Application (PDF)](../f19/clients/motorcycle-hud.jim-mahaney.pdf)
