---
title: Lecture recordings available
date: 2019-08-24
---

I've posted [recordings from this week's
lectures](https://www.youtube.com/channel/UC2G5pn05Fcw7CHjNJ3lOIcg/) on
YouTube.

<!--more-->

I'll try to record my lectures and get them online promptly this semester. I
expect that most students will be coming to class anyway. So this is as much
for my own reflective learning as for anybody else. But if you benefit from
them being available, so much the better.

Unfortunately, the second video is only 6 minutes long. The mic battery died,
and I decided there wasn't much point in uploading a bunch of video with no
audio.
