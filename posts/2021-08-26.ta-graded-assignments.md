---
title: TA Graded Assignments
date: 2021-08-26
---

As of the Fall '21 semester, assignments will be assessed by the TA rather than
each mentor. Also, after the walking skeleton is due, demo videos of new
features will be due each week. Read on for details.

<!--more-->

### New assignment grading policy

When I offered this course in the past, I asked mentors to give a rough
assessment of the assignment due each week. This assessment only had 4 options:

- **Unavailable**: it was not available
- **Incomplete**: available but did not meet requirements
- **Acceptable**: met requirements
- **Excellent**: exceeded requirements or of especially high quality

This approach worked, but there were a few problems:

1. Mentors assessed projects differently (which I attempted to account for when
   assigning grades).
2. Assignments were due at different times for each team, depending on when the
   mentor meeting was.
3. Mentors had to put on a sort of "management" hat for a minute, which goes
   against the open environment that I hope for in mentor meetings and could
   discourage teams from being honest with their mentor.

This semester, after brainstorming options with TA Vraj, I want to try having
the TA grade assignments each Monday morning. (Assignments will be due Mondays
at 8am unless otherwise noted.) I believe this will solve all of the above
problems.

The grading is still pretty coarse grained to make grading easier and avoid
needless distinctions: there will be a 2-point grading scale reflecting the
above definitions. A qualitative grade of excellent will be worth 3 points,
a.k.a. 150%. The syllabus has been updated. What the syllabus emphasizes bears
repeating: **Meeting all assignment requirements on time is the easiest way to
improve your grade in this course.**

If you earn a 0 or 1 on an assignment, and you address all requirements and
feedback by the following week, you can earn half the lost points back.

Each week will have equal weight in determining the assignment grade. So when
two assignments are due in one week, the individual scores are averaged to
compute the assignment grade for the week. The total assignment grade is the
average of all the weeks.

Assignments 0 ([project preferences](../../../../../project-preferences/)) and
15 ([personal report](../../../../../personal-report/)) are not graded so do not
contribute to the team's assignment grade. Assignment 10 ([tech
talk](../../../../../tech-talk/)) remains independent of the assignment grade.

### Demo videos

After [Assignment 8: Walking Skeleton](../../../../../walking-skeleton/), I
expect teams to finish not just whatever assignments are due each week (most of
which are pretty lightweight), but also to work on adding features. I asked
students to demo their progress each week to their mentor. This also felt more
like a management task than a mentor task. Furthermore, there was no explicit
grade for this work.

So, starting this year, after A8 is due, I'll expect teams to post demo videos
to their web site each week demonstrating a completed (and deployed, if
applicable) feature. These videos will be graded on the same 2-point scale as
assignments and averaged alongside whatever assignments are due to produce the
weekly grade. A video earning 2 points clearly demonstrates the feature in a way
that the client can reproduce.

You can read more about [demo videos](../../../../../demo-videos/), including
some tips for recording them.

I hope that these tweaks make COMP 523 even more effective at giving students
real-world experience building a product for a real client that they're proud
of.
