---
title: Updated midterm presentation assignment
date: 2019-10-09
---

I changed a couple of details about the [midterm presentation
assignment](../../../../../midterm-presentation/). Details follow.

<!--more-->

First, since there are fewer teams than I was expecting initially, we have more
time available per presentation. I still want to keep it at 9 minutes max,
though. And shorter is always fine. :-)

Second, all presentations need to be delivered from the classroom computer, so
that we can record everything. (This is less important for Monday, but Monday
is a trial run for Wednesday, when I will be out of town and will rely on the
recording.) This means that the presentation files and the clickable prototype
should not only be linked from your project web site, but also available for
anybody with the link to access. Please ensure that this is the case, to avoid
technical difficulties in class. Thanks!
