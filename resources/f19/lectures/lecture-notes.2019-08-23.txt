- taxonomy of work
  - non-creative work, e.g. doing point-of-sale at a grocery store
  - creative work, where you're creating something
  - spectrum of uncertainty with creative work
    - highly repetitive, highly certain creative work
      - e.g. an assembly line, creating the same widget every 10 seconds
      - building the same house plan multiple times, or apartments or townhomes
    - highly uncertain creative work
      - e.g. starting a business, especially for the first time
    - note: uncertainty is relative
  - many software engineers live and thrive on the more uncertain end of the
    spectrum
  - the perpetual newness of the work can be enjoyable, at least for some
  - you will definitely have some (maybe many!) areas of uncertainty in your
    project
  - major question: how to deal with uncertainty in your project?
  - let's leave that question dangling for a bit, and we'll come back to it
- how to divide work?
  - in 2 weeks, you'll be working on your user stories
  - getting a set of desires from your client
  - how do you approach that list of things to do? what do you do first?
  - there are many ways to divide the work
  - important to realize: you don't have to work one user story at a time
  - some work might be foundational to multiple stories
  - other work might accomplish one piece from multiple stories
  - other work isn't captured by a story at all
  - one example: sort the user stories alphabetically, and do them in that order
  - probably a bad way to do it
  - there are many ways to slice up the work, just like there are many ways to
    eat a Reeses...or triage your inbox...or (example?)
  - point is, it's a large body of work that can be sliced various ways
  - so what's a good way to start the work?
  - discuss amongst yourselves (3 minutes) (TODO: do I need a more concrete
    example?)
  - anybody hear a good insight from a classmate?
  - story about my failed AP CS project in HS
  - lesson: that wasn't a good way to divide the project
  - why? prioritized the shiny instead of the functional
  - also, teammates' work depended on mine, so the whole project failed to
    create anything even remotely usable
- my recommendation (start with most uncertain thing)
  - human nature: do the easy stuff, esp. if you can feel productive while doing
    it
  - this is why so many PhD students working on their dissertations have such
    clean apartments!
  - my recommendation: tackle the most uncertain things first
  - why? because then you know where you're going and how to get there!
  - (and have a much clearer sense of how long it will take, which is important
    to manage your own and your client's expectations)
  - note: different kinds of hard, and uncertain-hard can be especially hard to
    start on, because you don't know where to start
- spikes
  - recall that you don't need to proceed one user story at a time
  - a major source of uncertainty is uncertainty of integration or interface
  - like, how can I get the data I need? Is there an API I need to learn?
  - or how can I connect this thing to that thing?
  - example: client says it would be nice to create a task by bcc'ing an email
    address associated with the service.
  - so you'd need to integrate an email system with your database of tasks
    somehow, making the two able to talk to and affect each other
  - this calls for a "spike"
  - “A spike solution is a very simple program to explore potential
    solutions.” - http://agiledictionary.com/209/spike/
  - ‘What is the simplest thing we can program that will convince us we are on
    the right track?’ -Ward Cunningham
  - idea when integrating: building a bridge across a canyon, to connect the two
    sides
  - a spike is just a rope. the full implementation will be a bridge, but the
    spike just proves that it's possible to connect the two.
  - so for email example, a successful spike might be sending an email to a mail
    server that you set up, and being able to print "I saw an email" from code
    that you hooked into the mail server
  - it doesn't need to talk to a database: you already know that's possible
  - the point is to tackle uncertainty, and the thing you were uncertain about
    was being able to notice and respond to an inbound email
  - the goal here is depth, not breadth; hence the word "spike", which is all
    about depth
  - breadth can come later
  - also, the code can be very messy
  - this is short-lived code merely proving a possibility, so you can optimize
    for writing, not future reading
- planning & exploration phase
  - the phase of the project when most of the work you're doing is spikes is
    called "planning & exploration"
  - (at least, that's what we called it at RoleModel)
  - after P&E phase, the biggest uncertainties have been resolved
  - imagine working on a project where there were only minor
    uncertainties...that's one reason to do a P&E phase, to get the hard work
    out of the way first
  - at RoleModel, the process was marketing -> sales -> contract -> P&E ->
    development -> maintenance, and P&E was the first paid work
  - so the sales effort would include work usually by our software director to
    estimate how much time a P&E phase would take
  - whatever uncertainty we could address in that time, we would
  - whereas the goal of the development phase is working software, the goal of
    the P&E phase is information about what's feasible or recommended
  - the output of the P&E phase is a set of more concrete tasks, ideally broken
    down into 4-8 hour chunks, and estimates
  - these would form the basis for the development contract, with a specific
    budget
- case study: a P&E phase I did
  - data already extensively embedded in Google Cloud Platform (and nobody at
    our company knew GCP)
  - our uncertainty: can we deliver a web app via GCP that can send email at
    scheduled times and that's HIPAA compliant?
  - one spike: which GCP services are HIPAA compliant? Can we do everything we
    need with those? Where there's a choice between options, which option should
    we choose?
  - another spike: how do we efficiently deploy software to GCP?
  - another: how can we start some process at the same time every weekday?
  - I had a mediocre customer demo on that one because we couldn't get email
    sending to work...turns out Google App Engine silently ignores email sending
    from its apps, without any error notification...thanks.
- own your uncertainty
  - remember: uncertainty is relative: you might be uncertain about something
    I wouldn't be
  - relying on somebody else's opinions is a common way to resolve uncertainty
  - the trick is who to listen to, because there are a lot of opinions online
  - but googling is a totally valid way to resolve at least certain types of
    uncertainty
  - but if you need to dig deeper, remember to own your uncertainty: what
    matters is that you learn what you need to, so take your time and learn it
- types of uncertainty
  - integration, e.g. "how can I access the data I need?" or "how can I execute
    the effects I need?"
  - technology, e.g. "how easy or hard is it to do websockets in Rust?" or "I've
    heard good things about Flutter. Will it work out well for our project?"
  - performance, e.g. "how many elements can I have in an HTML page before it
    starts to bog down?"
  - data modeling or representation, e.g. "how can I represent this complex
    information in a database?"
- exercise
  - this will be a group exercise, for groups of 2 or 3
  - in your project, say you need to support user authentication somehow
  - your client says they think Google logins would be cool. Like the "login via
    Google" option that maybe you've seen somewhere.
  - (does anybody know how to do Google logins? I'm assuming you don't.)
  - so you're uncertain how much work that is.
  - how do you tackle the uncertainty here? what's your approach?
  - remember: uncertainty is relative. Figure this out given your actual
    knowledge level.
- tips for self-learning by Bradford Cross and Hamilton Ulmer
  - https://gist.github.com/gbarrancos/3277138
  - or https://web.archive.org/web/20101102204400/http://www.measuringmeasures.com/blog/2010/4/19/7-tips-for-successful-self-learning.html
  - 1. sometimes the shortest path is the long road: slow down, take your time,
    read it straight through, and absorb what's there
  - or: shortcuts on the fundamentals can come back to bite you in the long run
  - 2. avoid isolation: ask an expert; form a study group; find a community
    where you can ask questions (e.g. App Lab)
  - 3. avoid multitasking: silence your phone and turn off your email and chat
    clients for an hour or two. Maybe even find a quiet place and put your
    AirPods away.
  - 4. _work_ through books or manuals, don't just _read_ them: wrestle with the
    text and the exercises. Actually write the code in the tutorial.
  - 6. what to do when you don't understand: the "I'm stuck" decision tree
    - "Learning is all about abstractions. We build up abstractions on top of
      other abstractions. If you do not know the abstractions you are reading
      about that are being composed into new higher level abstractions, then you
      aren't going to understand the new abstraction. If you get stuck, the way
      to get un-stuck is to follow the I'm stuck decision tree below."
    - you are familiar with the abstractions you are seeing but...
      - you can't understand how they are being composed to form a new
        abstraction.
        - what you are reading is a poor treatment of the material -> find a
          better treatment
        - you are rusty with the abstractions -> go review and come back
        - you are fine with the abstractions and the treatment seems clear ->
          work through it to develop an intuition.
    - you are unfamiliar with the abstractions you are seeing
      - you know the field, but not this particular concept -> learn the concept
        and review other parts of the field on demand
      - you are unfamiliar with the concepts, terminology, and symbols; you
        don't even know what field this is part of -> you will have to step back
        from your current studies, find out what field you are in, and go learn
        a foundation in that field. (keep in mind that you often just need to
        build a general foundation in the field, or mastery of some subset of a
        field - you don't have to master the entire field.)
